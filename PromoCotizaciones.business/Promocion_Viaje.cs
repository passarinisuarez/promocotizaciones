using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.IO;
using passarini.web.business.transacciones;

namespace PromoCotizaciones.business
{
	/// <summary>
	/// Representa a una promoción de tipo viaje
    /// Metodos marcados con ** son utilizados SOLO en el sitio web de passarini.
	/// </summary>
    public class Promocion_Viaje : Base
	{
        //campos promocion general
		public int codigoPromocion;        
        public string operador;
        public int codigo_asesor;      

        /// <summary>
        /// Constructor de la clase. Recibe un contexto de conexión a base de datos.
        /// </summary>
		public Promocion_Viaje(BaseContext baseContext, int codigoPromocion) : base(baseContext)
		{
            try
            {
                base.beginBaseTransaction();

                this.codigoPromocion = codigoPromocion;

			    Promociones promociones = new Promociones(this.getBaseContextForNewInstance());
                DataTable dt = promociones.listarPromocionesViaje(codigoPromocion);
			    DataRow dr;
                if (dt.Rows.Count == 0)
                {
                    throw new Exception("No se ha encontrado la promoción: " + codigoPromocion);
                }
                dr = dt.Rows[0];
                this.codigoPromocion = int.Parse(dr["Codigo_Promocion"].ToString());
                this.operador = (string)dr["Operador"];

                if (dr["Codigo_Asesor"] is DBNull) { this.codigo_asesor = -1; }
                else { this.codigo_asesor = int.Parse(dr["Codigo_Asesor"].ToString()); }
                
                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

	}
}
