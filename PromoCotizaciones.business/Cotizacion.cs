﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using passarini.web.business.transacciones;


namespace PromoCotizaciones.business
{
    public class Cotizacion : Base
    {

        public int codigo_cotizacion;
        public string codigo_promocion;
        public string nombre;
        public string telefono;
        public string email;
        public string ip;
        public string texto_solicitud;
        public char via;
        public char status;
        public int num_adultos;
        public int num_ninos;
        public int num_infantes;
        public int codigo_asesor;


        public const char statusSolicitada = 'S';
        public const char statusCotizada = 'C';
        public const char statusAnulada = 'A';
        public const char statusCompraRealizada = 'R';

        public const char viaInternet = 'I';
        public const char viaPresencial = 'P';


        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public Cotizacion(BaseContext baseContext, int codigo_cotizacion)
            : base(baseContext)
        {
            try
            {
                base.beginBaseTransaction();

                this.codigo_cotizacion = codigo_cotizacion;
                
                SqlCommand com;
                SqlDataReader sqldr;

                com = base.getBaseCommand("select * from Cot_Cotizacion where Codigo_Cotizacion = @codigo_cotizacion;");
                com.Parameters.Add("@codigo_cotizacion", SqlDbType.VarChar).Value = codigo_cotizacion;
                sqldr = com.ExecuteReader();
                sqldr.Read();


                this.codigo_promocion = sqldr["codigo_promocion"] != null ? sqldr["codigo_promocion"].ToString() : "" ;
                this.nombre = (string)sqldr["nombre"];
                this.telefono = (string)sqldr["telefono"];
                this.email = (string)sqldr["email"];
                this.ip = (string)sqldr["ip"];
                this.texto_solicitud = (string)sqldr["texto_solicitud"];
                this.via = (char)sqldr["via"].ToString()[0];
                this.status = (char)sqldr["status"].ToString()[0];
                this.num_adultos = (int)sqldr["Numero_Adultos"];
                this.num_ninos = (int)sqldr["Numero_Ninos"];
                this.num_infantes = (int)sqldr["Numero_Infantes"];
                this.codigo_asesor = (int)sqldr["codigo_asesor"];

                sqldr.Close();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        

    }
}
