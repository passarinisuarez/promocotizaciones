﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using passarini.web.business.transacciones;

namespace PromoCotizaciones.business
{
    /// <summary>
    /// Maneja los colores de los titulos en las promociones que son banner
    /// Metodos marcados con ** son utilizados SOLO en el sitio web de passarini.
    /// </summary>
    public class Color_Titulo
    {

        public List<string> colores = new List<string>();

        public const string blanco = "white";
        public const string azul = "blue";
        public const string naranja = "orange";
        public const string amarillo = "e4ff00";
        public const string gris = "c6c6c6";
        public const string negro = "black";

        public Color_Titulo()
        {
            colores.Add(blanco); colores.Add(azul); colores.Add(naranja); colores.Add(amarillo); colores.Add(gris); colores.Add(negro);
        }

        public static string NombreColor(string valor)
        { 
            switch (valor){
                case blanco:
                    return "Blanco";
                case azul:
                    return "Azul";
                case naranja:
                    return "Naranja";
                case amarillo:
                    return "Amarillo";
                case gris:
                    return "Gris";
                case negro:
                    return "Negro";
                default:
                    return "";
            }
        }

        /// <summary>
        /// Metodo que retorna la clase para el ccs que debera tomar forma
        /// en el website
        /// </summary>
        public static string CCSClass(string valor)
        {
            switch (valor)
            {
                case blanco:
                    return "textoBlanco";
                case azul:
                    return "textoAzul";
                case naranja:
                    return "textoNaranja";
                case amarillo:
                    return "textoAmarillo";
                case gris:
                    return "textoGris";
                case negro:
                    return "textoNegro";
                default:
                    return "";
            }
        } 

    }
}
