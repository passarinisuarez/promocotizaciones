using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.IO;
using passarini.web.business.transacciones;

namespace PromoCotizaciones.business
{
	/// <summary>
	/// Representa a una promoción
	/// </summary>
    public class Promocion : Base
	{
        //campos promocion general
		public int codigoPromocion;
        public int codigoClasificacion;
        public int codigoSubclasificacion;
        public int tipo;
        public String grupo;
        public int orden;
        public String tarifaConsolidadaMoneda;
        public float tarifaConsolidada;
        public enumTipoTarifa tarifaConsolidadaTipo;
        public string tipo_tarifa_consolidada;
        public String titulo;
        public String subtitulo;
        public String link;
        public DateTime validaDesde;
        public DateTime validaHasta;
        public DateTime publicacion_desde;
        public DateTime publicacion_hasta;
        public bool activa;
        public bool destacado;
        public string seo_url;
        public string color_titulo;
        public DataTable tarifa;

        //campos tipo de promocion. tipo = 2 viajes
        public Promocion_Viaje promo_viaje;

        public enum enumTipoTarifa
        {
            Llamar = 'L',
            Desde = 'D',
            Total = 'T',
            VerDetalle = 'V',
            PDF = 'P'
        }

        private int valorTipoTarifa(char valor){
            switch (valor)
            {
                case 'P':
                    return 1;
                case 'D':
                    return 2;
                case 'L':
                    return 3;
                case 'V':
                    return 4;
                default:
                    return -1;
            }
        }

        /// <summary>
        /// Constructor de la clase. Recibe un contexto de conexión a base de datos.
        /// </summary>
		public Promocion(BaseContext baseContext, int codigoPromocion) : base(baseContext)
		{
            try
            {
                base.beginBaseTransaction();

                this.codigoPromocion = codigoPromocion;

                Promociones promociones = new Promociones(baseContext);
                DataTable dt = promociones.listarPromociones(codigoPromocion);
                
			    DataRow dr;
			    if (dt.Rows.Count == 0) 
			    {
                    
				    throw new Exception("No se ha encontrado la promoción: " + codigoPromocion);
                    
			    }
			    dr = dt.Rows[0];
                this.codigoPromocion = int.Parse(dr["Codigo_Promocion"].ToString());
                this.codigoClasificacion = int.Parse(dr["Codigo_Clasificacion"].ToString());
                if (dr["Codigo_Subclasificacion"] is DBNull)
                {
                    this.codigoSubclasificacion = -1;
                }
                else
                {
                    this.codigoSubclasificacion = int.Parse(dr["Codigo_Subclasificacion"].ToString());
                }
                if (dr["Tipo"] is DBNull){ this.tipo = -1; }
                else { this.tipo = int.Parse(dr["Tipo"].ToString()); }

                if (dr["Tarifa_Consolidada_Moneda"] is DBNull){ this.tarifaConsolidadaMoneda = ""; }
                else { this.tarifaConsolidadaMoneda = dr["Tarifa_Consolidada_Moneda"].ToString(); }
                
                if (dr["Grupo"] is DBNull){ this.grupo = ""; } 
                else { this.grupo = dr["Grupo"].ToString(); }

                if (dr["Orden"] is DBNull) { this.orden = -1; }
                else { this.orden = int.Parse(dr["Orden"].ToString()); }

                if (dr["Tarifa_Consolidada"] is DBNull)
                {
                    this.tarifaConsolidada = -1;
                }
                else
                {
                    this.tarifaConsolidada = float.Parse(dr["Tarifa_Consolidada"].ToString());
                }
                this.tarifaConsolidadaTipo = (enumTipoTarifa)dr["Tarifa_Consolidada_Tipo"].ToString()[0];
                this.tipo_tarifa_consolidada = dr["Tarifa_Consolidada_Tipo"].ToString();
                this.titulo = (String)dr["Titulo"];
                this.subtitulo = (String)dr["Subtitulo"];

                if (dr["Seo_Url"] == DBNull.Value) { this.seo_url = ""; }
                else { this.seo_url = (String)dr["Seo_Url"]; }

                if (dr["Link"] == DBNull.Value) { this.link = ""; }
                else { this.link = (String)dr["Link"]; }

                this.validaDesde = (DateTime)dr["Valida_Desde"];
                this.validaHasta = (DateTime)dr["Valida_Hasta"];
                this.publicacion_desde = (DateTime)dr["Publicacion_Desde"];
                this.publicacion_hasta = (DateTime)dr["Publicacion_Hasta"];
                this.activa = (bool)dr["Activa"];
                this.destacado = (bool)dr["Destacado"];
                this.color_titulo = (string)dr["Color_Titulos"];
                dt = promociones.listarTarifa(codigoPromocion);
                this.tarifa = dt;

                if (Tipo_Promocion.esPromoViaje(this.tipo))
                {
                    this.promo_viaje = new Promocion_Viaje(baseContext, this.codigoPromocion);
                }

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }       

        //public List<String> listarAttachments(String carpetaAttachments)
        //{
        //    List<String> attachments = new List<String>();
        //    string[] attStr[] = Directory.GetFiles(carpetaAttachments, "attachment_" + this.codigoPromocion.ToString() + ".*";


        //    return attachments;
        //}

	}
}
