using System;
using System.Data;
using System.Data.SqlClient;
using passarini.web.business.transacciones;

namespace PromoCotizaciones.business
{
	/// <summary>
	/// Representa a un agente
	/// </summary>
    public class Asesor : Base
	{
		public int codigoAsesor;
		public String nombre;
        public bool es_supervisor;

		

        /// <summary>
        /// Constructor de la clase. Recibe un contexto de conexi�n a base de datos.
        /// </summary>
		public Asesor(BaseContext baseContext, int codigoAsesor) : base(baseContext)
		{
            try
            {
                base.beginBaseTransaction();

                if (codigoAsesor < 0) 
			    {
				    throw new Exception("No se ha encontrado el Asesor");
			    }
                this.codigoAsesor = codigoAsesor;

			    Auxiliares auxiliares = new Auxiliares(this.getBaseContextForNewInstance());
                DataTable dt = auxiliares.listarAsesores(codigoAsesor);
			    DataRow dr;
			    if (dt.Rows.Count == 0) 
			    {
                    throw new Exception("No se ha encontrado el Asesor: " + codigoAsesor);
			    }
			    dr = dt.Rows[0];
			    this.nombre = (String)dr["Nombre"];
                this.es_supervisor = (bool)dr["Supervisor"];

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Actualiza en la base de datos los valores de las propiedades en memoria
        /// </summary>
        public void actualizarCampos(string nombre_nvo, bool supervisor)
        {
            try
            {
                base.beginBaseTransaction();
                Auxiliares auxiliares = new Auxiliares(this.getBaseContextForNewInstance());                
                SqlCommand com;
                
                com = this.getBaseCommand("update aux_asesor set nombre = @nombre_nvo, supervisor = @supervisor where codigo_asesor = @codigo_asesor;");
                ((SqlParameter)com.Parameters.Add("@codigo_asesor", SqlDbType.Int)).Value = this.codigoAsesor;
                ((SqlParameter)com.Parameters.Add("@nombre_nvo", SqlDbType.VarChar)).Value = nombre_nvo;
                ((SqlParameter)com.Parameters.Add("@supervisor", SqlDbType.Bit)).Value = supervisor;
                
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }
		
		 
	}
}
