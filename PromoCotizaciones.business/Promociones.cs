using System;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Net.Mime;
using passarini.web.business.transacciones;

namespace PromoCotizaciones.business
{
	/// <summary>
	/// Maneja las promociones. 
    /// Metodos marcados con ** son utilizados SOLO en el sitio web de passarini.
	/// </summary>
    public class Promociones : Base
	{

        // ** estas const son de sitio web
        public const int email_turismo = 1;
        public const int email_ventas = 2;
        public const int email_corporativo = 3;
        public const int email_newsletter = 4;
        public const int email_quiero_qm_llamen = 5;

        // ** estas const son de sitio web
        public const int clas_bannerSup = 1;
        public const int clas_precios = 2;
        public const int clas_mapa = 3;
        public const int clas_cruceros = 4;
        public const int clas_especiales = 5;
        public const int clas_servicios = 6;
        public const int clas_hotelescorporativos = 7;
        public const int clas_fyers = 8;


        /// <summary>
        /// Constructor de la clase. Recibe un contexto de conexión a base de datos.
        /// </summary>
        public Promociones(BaseContext baseContext) : base(baseContext)
		{
            try
            {
                base.beginBaseTransaction();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }
        
        /// <summary>
        /// Lista TODAS las promociones sin filtros
        /// </summary>
        public DataTable listarSoloModificadas()
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds;

                ad = this.getBaseDataAdapter("select * from pro_promocion where modificada = '1' order by Activa DESC, codigo_promocion DESC, Tarifa_Consolidada ASC;");
                ds = new DataSet();
                ad.Fill(ds, "Tabla");

                base.commitBaseTransaction();

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Lista las promociones de acuerdo a los filtros dados
        /// </summary>
        public DataTable listarPromociones(bool soloFechasDePublicacionVigentes = true)
        {
            try
            {
                base.beginBaseTransaction();

                DataTable temp = this.listarPromociones(-1, -1, -1, "", "", "", soloFechasDePublicacionVigentes);

                base.commitBaseTransaction();

                return temp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Lista las promociones de acuerdo a los filtros dados
        /// </summary>
        public DataTable listarPromociones(bool soloFechasDePublicacionVigentes = true, bool es_promocot = false)
        {
            try
            {
                base.beginBaseTransaction();

                DataTable temp = this.listarPromociones(-1, -1, -1, "", "", "", soloFechasDePublicacionVigentes, es_promocot);

                base.commitBaseTransaction();

                return temp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Lista las promociones de acuerdo a los filtros dados
        /// </summary>
        public DataTable listarPromociones(int codigoPromocion)
        {
            try
            {
                base.beginBaseTransaction();

                DataTable temp = this.listarPromociones(codigoPromocion, -1, -1, "", "", "", false);

                base.commitBaseTransaction();

                return temp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Lista las promociones de acuerdo a los filtros dados
        /// </summary>
        public DataTable listarPromociones(string grupo)
        {
            try
            {
                base.beginBaseTransaction();

                DataTable temp = this.listarPromociones(-1, -1, -1, grupo, "", "");

                base.commitBaseTransaction();

                return temp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Lista las promociones de acuerdo a los filtros dados
        /// </summary>
        public DataTable listarPromociones(int codigoClasificacion, String texto)
        {
            try
            {
                base.beginBaseTransaction();

                DataTable temp = this.listarPromociones(-1, codigoClasificacion, -1, "", texto, "");

                base.commitBaseTransaction();

                return temp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Lista las promociones de acuerdo a los filtros dados
        /// </summary>
        public DataTable listarPromociones(int codigoPromocion, int codigoClasificacion, int codigoSubclasificacion, String grupo, String texto, String orden, bool soloVigentes = true, bool es_promocot = false)
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds;
                String busqueda_default = "(case when p.orden < 0 then 2 else 1 end), p.orden, p.Tarifa_Consolidada ASC";
                String busqueda_promocot = "(case when p.orden < 0 then 2 else 1 end), p.Codigo_promocion DESC, p.orden, p.Tarifa_Consolidada ASC";
                String ordenadoPor = (orden != "" ? orden : (es_promocot ? busqueda_promocot : busqueda_default));

                //grupos separados por comas, hace match con un solo grupo que haga match
                string[] grupos = grupo.Split(',');
                String busqueda_grupos = "";
                if (grupo != "")
                {
                    foreach (string g in grupos)
                        busqueda_grupos = busqueda_grupos + (busqueda_grupos == "" ? "" : " or ") + "p.grupo like '%" + g + "%'";

                    if (busqueda_grupos != "")
                        busqueda_grupos = "(" + busqueda_grupos + ") and";
                }

            //ordenes positivos
            ad = this.getBaseDataAdapter(
                    "select p.* " +
                    "from pro_promocion p " +
                    "where (codigo_promocion = @codigo_promocion or @codigo_promocion < 0) and " +
                    "(codigo_clasificacion = @codigo_clasificacion or @codigo_clasificacion < 0) and " +
                    "(codigo_subclasificacion = @codigo_subclasificacion or @codigo_subclasificacion < 0) and " +
                    (soloVigentes ? "publicacion_desde < getdate() and dateadd(d,1,publicacion_hasta) > getdate() and" : "") +
                    busqueda_grupos + 
                    " (p.titulo+p.grupo+p.subtitulo like '%' + @texto + '%' COLLATE SQL_LATIN1_GENERAL_CP1_CI_AI) " +
                    (soloVigentes ? "and activa = 1 " : "") + " order by " + ordenadoPor + " ;");
                ((SqlParameter)ad.SelectCommand.Parameters.Add("@codigo_promocion", SqlDbType.Int)).Value = codigoPromocion;
                ((SqlParameter)ad.SelectCommand.Parameters.Add("@codigo_clasificacion", SqlDbType.Int)).Value = codigoClasificacion;
                ((SqlParameter)ad.SelectCommand.Parameters.Add("@codigo_subclasificacion", SqlDbType.Int)).Value = codigoSubclasificacion;
                ((SqlParameter)ad.SelectCommand.Parameters.Add("@texto", SqlDbType.VarChar)).Value = texto;
                ds = new DataSet();
                ad.Fill(ds, "Tabla");

                base.commitBaseTransaction();

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Lista las promociones de acuerdo a los filtros dados
        /// lista sobre EL TOTAL DE PROMOCIONES
        /// </summary>
        public DataTable listarPromociones(bool solo_validas, bool solo_publicadas, bool solo_activas)
        {
            try
            {
                base.beginBaseTransaction();

                DataTable temp = this.listarPromociones(new DateTime(2000, 1, 1), new DateTime(2000, 1, 1), new DateTime(2000, 1, 1), new DateTime(2000, 1, 1), 
                                                        -1, -1, solo_validas, solo_publicadas, solo_activas);

                base.commitBaseTransaction();

                return temp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }
        

        /// <summary>
        /// Lista las promociones de acuerdo a los filtros dados
        /// lista sobre EL TOTAL DE PROMOCIONES
        /// </summary>
        public DataTable listarPromociones(
            DateTime valida_desde, DateTime valida_hasta, DateTime publicacion_desde, DateTime publicacion_hasta, 
            int tipo, int codigo_asesor, bool solo_validas, bool solo_publicadas, bool solo_activas)
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds;

                //ordenes positivos
                ad = this.getBaseDataAdapter(
                    "select p.*  " +
                    "from Pro_Promocion AS p LEFT OUTER JOIN Pro_Promocion_Viaje AS p_v ON p.Codigo_Promocion = p_v.Codigo_Promocion " +
                    "where " +
                          (solo_validas ?
                              "(valida_desde < getdate() and dateadd(d,1,valida_hasta) > getdate()) and " :
                              "(valida_desde >= @valida_desde or @valida_desde = @null) and (valida_hasta <= @valida_hasta or @valida_hasta = @null) and ") +
                          //"(valida_desde >= @valida_desde or @valida_desde = @null) and " +
                          //"(valida_hasta <= @valida_hasta or @valida_hasta = @null) and " +
                          (solo_publicadas ?
                              "(publicacion_desde < getdate() and dateadd(d,1,publicacion_hasta) > getdate()) and " :
                              "(publicacion_desde >= @publicacion_desde or @publicacion_desde = @null) and (publicacion_hasta <= @publicacion_hasta or @publicacion_hasta = @null) and ") + 
                          //"(publicacion_desde >= @publicacion_desde or @publicacion_desde = @null) and " +
                          //"(publicacion_hasta <= @publicacion_hasta or @publicacion_hasta = @null) and " +
                          (solo_activas ? " Activa = 1 and " : "") +
                          "(tipo = @tipo or @tipo < 0) and " +
                          "(p_v.codigo_asesor = @codigo_asesor or @codigo_asesor < 0) " +
                          //"orden >= 0 " +
                    "order by (case when orden < 0 then 2 else 1 end), orden, p.Codigo_Promocion DESC;");

                ((SqlParameter)ad.SelectCommand.Parameters.Add("@valida_desde", SqlDbType.SmallDateTime)).Value = valida_desde;
                ((SqlParameter)ad.SelectCommand.Parameters.Add("@valida_hasta", SqlDbType.SmallDateTime)).Value = valida_hasta;
                ((SqlParameter)ad.SelectCommand.Parameters.Add("@publicacion_desde", SqlDbType.SmallDateTime)).Value = publicacion_desde;
                ((SqlParameter)ad.SelectCommand.Parameters.Add("@publicacion_hasta", SqlDbType.SmallDateTime)).Value = publicacion_hasta;
                ((SqlParameter)ad.SelectCommand.Parameters.Add("@null", SqlDbType.SmallDateTime)).Value = new DateTime(2000, 1, 1);
                ((SqlParameter)ad.SelectCommand.Parameters.Add("@tipo", SqlDbType.Int)).Value = tipo;
                ((SqlParameter)ad.SelectCommand.Parameters.Add("@codigo_asesor", SqlDbType.Int)).Value = codigo_asesor;

                ds = new DataSet();
                ad.Fill(ds, "Tabla");

                base.commitBaseTransaction();

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }
        

        /// <summary>
        /// Lista los valores de las promociones de viaje segun su codigo de promocion
        /// </summary>
        public DataTable listarPromocionesViaje(int codigo_promocion)
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds;

                ad = this.getBaseDataAdapter("select * from pro_promocion_viaje where codigo_promocion = @codigo_promocion order by codigo_promocion DESC;");
                ((SqlParameter)ad.SelectCommand.Parameters.Add("@codigo_promocion", SqlDbType.Int)).Value = codigo_promocion;
                ds = new DataSet();
                ad.Fill(ds, "Tabla");

                base.commitBaseTransaction();

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Lista las promociones de acuerdo a los filtros dados
        /// </summary>
        public DataTable listarPromocionesDestacadas()
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds;

                ad = this.getBaseDataAdapter("select * from pro_promocion where destacado = 1 and valida_desde < getdate() and dateadd(d,1,valida_hasta) > getdate() and publicacion_desde < getdate() and dateadd(d,1,publicacion_hasta) > getdate() and activa = 1 AND orden > 0 order by orden, Tarifa_Consolidada ASC;");
                ds = new DataSet();
                ad.Fill(ds, "Tabla");
                DataTable tmp = ds.Tables[0];

                ad = this.getBaseDataAdapter("select * from pro_promocion where destacado = 1 and valida_desde < getdate() and dateadd(d,1,valida_hasta) > getdate() and publicacion_desde < getdate() and dateadd(d,1,publicacion_hasta) > getdate() and activa = 1 AND orden <= 0 order by orden, Tarifa_Consolidada ASC;");
                ds = new DataSet();
                ad.Fill(ds, "Tabla");
                tmp.Merge(ds.Tables[0]);

                base.commitBaseTransaction();

                return tmp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Devuelve el codigo de promocion segun su seo_url
        /// </summary>
        public int obtenerCodigoPorSeoUrl(string seo_url)
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds;

                ad = this.getBaseDataAdapter("select p.codigo_promocion from pro_promocion p where (seo_url = @seo_url) and publicacion_desde < getdate() and dateadd(d,1,publicacion_hasta) > getdate() and activa = 1 order by p.orden, p.Tarifa_Consolidada ASC;");
                ((SqlParameter)ad.SelectCommand.Parameters.Add("@seo_url", SqlDbType.VarChar)).Value = seo_url;
                ds = new DataSet();
                ad.Fill(ds, "Tabla");

                base.commitBaseTransaction();

                if (ds.Tables[0].Rows.Count == 0)
                {
                    return -1;
                }
                else
                {
                    return int.Parse(ds.Tables[0].Rows[0]["codigo_promocion"].ToString());
                }

            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Lista las tarifas detalladas de la promoción dada
        /// </summary>
        public DataTable listarTarifa(int codigoPromocion)
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds;

                ad = this.getBaseDataAdapter("select * from pro_promocion_tarifa where codigo_promocion = @codigo_promocion order by orden_tarifa;");
                ((SqlParameter)ad.SelectCommand.Parameters.Add("@codigo_promocion", SqlDbType.Int)).Value = codigoPromocion;
                ds = new DataSet();
                ad.Fill(ds, "Tabla");

                base.commitBaseTransaction();

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Selecciona la ultima tarifa y devuelve su codigo.
        /// </summary>
        public int seleccionarUltimaTarifa(int codigo_promocion)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com = this.getBaseCommand();
                int temp;

                com.CommandText = "SELECT TOP 1 Orden_Tarifa FROM Pro_Promocion_Tarifa where codigo_promocion = @codigo_promocion order by Orden_Tarifa DESC;";
                com.Parameters.Add("@codigo_promocion", SqlDbType.Int).Value = codigo_promocion;
                temp = ((int)com.ExecuteScalar());

                base.commitBaseTransaction();

                return temp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Registra una tarifa de cotizacion
        /// </summary>
        public void registrarTarifaPromocion(int codigo_promocion, string descripcion, string moneda, float monto, int orden)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("insert into Pro_Promocion_Tarifa "
                                         + "(Codigo_Promocion, Descripcion, Codigo_Moneda, Monto, Orden_tarifa) values "
                                         + "(@codigo_promocion, @descripcion, @moneda, @monto, @orden);");
                com.Parameters.Add("@codigo_promocion", SqlDbType.Int).Value = codigo_promocion;
                com.Parameters.Add("@descripcion", SqlDbType.VarChar).Value = descripcion;
                com.Parameters.Add("@moneda", SqlDbType.VarChar).Value = moneda;
                com.Parameters.Add("@monto", SqlDbType.Float).Value = monto;
                com.Parameters.Add("@orden", SqlDbType.Int).Value = orden;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Elimina una tarifa
        /// </summary>
        public void eliminarTarifa(int codigo_promocion, int orden_tarifa)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand();
                com.CommandText = "delete from Pro_Promocion_Tarifa where codigo_promocion = @codigo_promocion and orden_tarifa = @orden_tarifa;";
                com.Parameters.Add("@codigo_promocion", SqlDbType.Int).Value = codigo_promocion;
                com.Parameters.Add("@orden_tarifa", SqlDbType.Int).Value = orden_tarifa;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Actualiza datos de una tarifa
        /// </summary>
        public void actualizarTarifa(int codigo_promocion, string descripcion, string moneda, float monto, int orden)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("update Pro_Promocion_Tarifa set " +
                                          "descripcion = @descripcion, " +
                                          "Codigo_Moneda = @moneda, " +
                                          "monto = @monto " +
                                          "where codigo_promocion = @codigo_promocion and Orden_Tarifa = @orden;");
                com.Parameters.Add("@codigo_promocion", SqlDbType.Int).Value = codigo_promocion;
                com.Parameters.Add("@descripcion", SqlDbType.VarChar).Value = descripcion;
                com.Parameters.Add("@moneda", SqlDbType.VarChar).Value = moneda;
                com.Parameters.Add("@monto", SqlDbType.Float).Value = monto;
                com.Parameters.Add("@orden", SqlDbType.Int).Value = orden;

                com.ExecuteNonQuery();

                base.commitBaseTransaction();

            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Lista los grupos posibles
        /// </summary>
        public DataTable listarGrupos()
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds;

                ad = this.getBaseDataAdapter("SELECT DISTINCT Grupo FROM Pro_Promocion ORDER BY Grupo;");
                ds = new DataSet();
                ad.Fill(ds, "Tabla");

                base.commitBaseTransaction();

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Devuelve el nombre del destino dado
        /// </summary>
        public String nombreDestino(int codigoSubclasificacion)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com = this.getBaseCommand("select nombre from pro_destino where codigo_subclasificacion = @codigo_subclasificacion;");
                com.Parameters.Add("@codigo_subclasificacion", SqlDbType.Int).Value = codigoSubclasificacion;
                String temp = (string)com.ExecuteScalar();

                base.commitBaseTransaction();

                return temp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Agrega una nueva promocion y devuelve su codigo.
        /// </summary>
        public int agregarPromocion(
            int codigo_promocion, int codigo_clasificacion, int codigo_subclasificacion, 
            int orden, string tarifa_moneda, decimal tarifa, string tipo_tarifa, string titulo,
            string subtitulo, DateTime valida_desde, DateTime valida_hasta, bool activa, string grupo, 
            string seo_url, string color_titulos, bool destacado, string link, int tipo,
            DateTime publicacion_desde, DateTime publicacion_hasta)
        {
            try
            {
                actualizarOrdenPromociones(orden);

                base.beginBaseTransaction();

                SqlCommand com = this.getBaseCommand();

                com.CommandText =
                    "insert into pro_promocion " +
                        "(Codigo_Promocion, Codigo_Clasificacion, Codigo_Subclasificacion," +
                        " Orden, Tarifa_Consolidada_Moneda, Tarifa_Consolidada," +
                        " Tarifa_Consolidada_Tipo, Titulo, Subtitulo, Valida_Desde, Valida_Hasta,"+
                        " Activa, Grupo, Seo_Url, Color_Titulos, Destacado, Link, Tipo, Publicacion_Desde, "+
                        " Publicacion_Hasta) " +
                    "values "+
                        "(@codigo_promocion, @codigo_clasificacion, @codigo_subclasificacion, "+
                        " @orden, @tarifa_moneda, @tarifa, @tipo_tarifa, @titulo, @subtitulo, @valida_desde," +
                        " @valida_hasta, @activa, @grupo, @seo_url, @color_titulos, @destacado, @link, @tipo,"+
                        " @publicacion_desde, @publicacion_hasta);";
                
                com.Parameters.Clear();
                ((SqlParameter)com.Parameters.Add("@codigo_promocion", SqlDbType.Int)).Value = codigo_promocion;
                ((SqlParameter)com.Parameters.Add("@codigo_clasificacion", SqlDbType.Int)).Value = codigo_clasificacion;
                ((SqlParameter)com.Parameters.Add("@codigo_subclasificacion", SqlDbType.Int)).Value = codigo_subclasificacion;                
                ((SqlParameter)com.Parameters.Add("@orden", SqlDbType.Int)).Value = orden;                
                ((SqlParameter)com.Parameters.Add("@tarifa_moneda", SqlDbType.VarChar)).Value = tarifa_moneda;
                ((SqlParameter)com.Parameters.Add("@tarifa", SqlDbType.Decimal)).Value = tarifa;
                ((SqlParameter)com.Parameters.Add("@tipo_tarifa", SqlDbType.VarChar)).Value = tipo_tarifa;
                ((SqlParameter)com.Parameters.Add("@titulo", SqlDbType.VarChar)).Value = titulo;
                ((SqlParameter)com.Parameters.Add("@subtitulo", SqlDbType.VarChar)).Value = subtitulo;
                ((SqlParameter)com.Parameters.Add("@valida_desde", SqlDbType.Date)).Value = valida_desde;
                ((SqlParameter)com.Parameters.Add("@valida_hasta", SqlDbType.Date)).Value = valida_hasta;
                ((SqlParameter)com.Parameters.Add("@publicacion_desde", SqlDbType.Date)).Value = publicacion_desde;
                ((SqlParameter)com.Parameters.Add("@publicacion_hasta", SqlDbType.Date)).Value = publicacion_hasta;
                ((SqlParameter)com.Parameters.Add("@activa", SqlDbType.Bit)).Value = activa;
                ((SqlParameter)com.Parameters.Add("@grupo", SqlDbType.VarChar)).Value = grupo;
                ((SqlParameter)com.Parameters.Add("@seo_url", SqlDbType.VarChar)).Value = seo_url;
                ((SqlParameter)com.Parameters.Add("@color_titulos", SqlDbType.VarChar)).Value = color_titulos;
                ((SqlParameter)com.Parameters.Add("@destacado", SqlDbType.Bit)).Value = destacado;
                ((SqlParameter)com.Parameters.Add("@link", SqlDbType.Text)).Value = link;
                ((SqlParameter)com.Parameters.Add("@tipo", SqlDbType.Int)).Value = tipo;                
                

                com.ExecuteNonQuery();

                base.commitBaseTransaction();

                return this.seleccionarUltimaPromocion();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Agrega una nueva promocion de tipo viajes y devuelve su codigo.
        /// </summary>
        public int agregarPromocion(
            int codigo_promocion, int codigo_clasificacion, int codigo_subclasificacion,
            int orden, string tarifa_moneda, decimal tarifa, string tipo_tarifa, string titulo,
            string subtitulo, DateTime valida_desde, DateTime valida_hasta, bool activa, string grupo,
            string seo_url, string color_titulos, bool destacado, string link, int tipo, 
            DateTime publicacion_desde, DateTime publicacion_hasta, string operador, int codigo_asesor)
        {
            try
            {
                actualizarOrdenPromociones(orden);

                base.beginBaseTransaction();

                SqlCommand com = this.getBaseCommand();

                com.CommandText =
                    "insert into pro_promocion " +
                        "(Codigo_Promocion, Codigo_Clasificacion, Codigo_Subclasificacion," +
                        " Orden, Tarifa_Consolidada_Moneda, Tarifa_Consolidada," +
                        " Tarifa_Consolidada_Tipo, Titulo, Subtitulo, Valida_Desde, Valida_Hasta," +
                        " Activa, Grupo, Seo_Url, Color_Titulos, Destacado, Link, Tipo, Publicacion_Desde, " +
                        " Publicacion_Hasta) " +
                    "values " +
                        "(@codigo_promocion, @codigo_clasificacion, @codigo_subclasificacion, " +
                        " @orden, @tarifa_moneda, @tarifa, @tipo_tarifa, @titulo, @subtitulo, @valida_desde," +
                        " @valida_hasta, @activa, @grupo, @seo_url, @color_titulos, @destacado, @link, @tipo," +
                        " @publicacion_desde, @publicacion_hasta);";

                com.Parameters.Clear();
                ((SqlParameter)com.Parameters.Add("@codigo_promocion", SqlDbType.Int)).Value = codigo_promocion;
                ((SqlParameter)com.Parameters.Add("@codigo_clasificacion", SqlDbType.Int)).Value = codigo_clasificacion;
                ((SqlParameter)com.Parameters.Add("@codigo_subclasificacion", SqlDbType.Int)).Value = codigo_subclasificacion;
                ((SqlParameter)com.Parameters.Add("@orden", SqlDbType.Int)).Value = orden;
                ((SqlParameter)com.Parameters.Add("@tarifa_moneda", SqlDbType.VarChar)).Value = tarifa_moneda;
                ((SqlParameter)com.Parameters.Add("@tarifa", SqlDbType.Decimal)).Value = tarifa;
                ((SqlParameter)com.Parameters.Add("@tipo_tarifa", SqlDbType.VarChar)).Value = tipo_tarifa;
                ((SqlParameter)com.Parameters.Add("@titulo", SqlDbType.VarChar)).Value = titulo;
                ((SqlParameter)com.Parameters.Add("@subtitulo", SqlDbType.VarChar)).Value = subtitulo;
                ((SqlParameter)com.Parameters.Add("@valida_desde", SqlDbType.Date)).Value = valida_desde;
                ((SqlParameter)com.Parameters.Add("@valida_hasta", SqlDbType.Date)).Value = valida_hasta;
                ((SqlParameter)com.Parameters.Add("@publicacion_desde", SqlDbType.Date)).Value = publicacion_desde;
                ((SqlParameter)com.Parameters.Add("@publicacion_hasta", SqlDbType.Date)).Value = publicacion_hasta;
                ((SqlParameter)com.Parameters.Add("@activa", SqlDbType.Bit)).Value = activa;
                ((SqlParameter)com.Parameters.Add("@grupo", SqlDbType.VarChar)).Value = grupo;
                ((SqlParameter)com.Parameters.Add("@seo_url", SqlDbType.VarChar)).Value = seo_url;
                ((SqlParameter)com.Parameters.Add("@color_titulos", SqlDbType.VarChar)).Value = color_titulos;
                ((SqlParameter)com.Parameters.Add("@destacado", SqlDbType.Bit)).Value = destacado;
                ((SqlParameter)com.Parameters.Add("@link", SqlDbType.Text)).Value = link;
                ((SqlParameter)com.Parameters.Add("@tipo", SqlDbType.Int)).Value = tipo;                
                com.ExecuteNonQuery();

                com.Parameters.Clear();
                com.CommandText =
                    "insert into pro_promocion_viaje " +
                        "(Codigo_Promocion, Operador, Codigo_Asesor) " +
                    "values " +
                        "(@codigo_promocion, @operador, @codigo_asesor);";                
                ((SqlParameter)com.Parameters.Add("@codigo_promocion", SqlDbType.Int)).Value = codigo_promocion;
                ((SqlParameter)com.Parameters.Add("@codigo_asesor", SqlDbType.Int)).Value = codigo_asesor;
                ((SqlParameter)com.Parameters.Add("@operador", SqlDbType.VarChar)).Value = operador;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();

                return this.seleccionarUltimaPromocion();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Selecciona la ultima promocion y devuelve su codigo.
        /// </summary>
        public int seleccionarUltimaPromocion()
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com = this.getBaseCommand();
                int temp;

                com.CommandText = "SELECT TOP 1 Codigo_Promocion FROM Pro_Promocion order by Codigo_Promocion DESC;";
                temp = ((int)com.ExecuteScalar());

                base.commitBaseTransaction();

                return temp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                return 0;
            }
        }

        /// <summary>
        /// Activa/Desactiva una promocion
        /// </summary>
        public void activar_desactivarPromocion(int codigo_promocion, bool activa)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("update pro_promocion set " +
                                          "Activa = @activa " +
                                          "where codigo_promocion = @codigo_promocion ;");
                com.Parameters.Add("@codigo_promocion", SqlDbType.Int).Value = codigo_promocion;
                com.Parameters.Add("@activa", SqlDbType.Bit).Value = activa;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();

            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Acualiza datos de una promocion
        /// </summary>
        public void actualizarPromocion(
            int codigo_promocion, int codigo_clasificacion, int codigo_subclasificacion, int orden,
            string titulo, string subtitulo, DateTime valida_desde, DateTime valida_hasta, string grupo, 
            string seo_url, string color_titulos, bool destacado, string link, 
            DateTime publicacion_desde, DateTime publicacion_hasta)
        {
            try
            {
                actualizarOrdenPromociones(orden);

                base.beginBaseTransaction();                

                SqlCommand com;

                com = base.getBaseCommand("update pro_promocion set " +
                                          "codigo_clasificacion = @codigo_clasificacion, " +
                                          "codigo_subclasificacion = @codigo_subclasificacion, " +
                                          "orden = @orden, "+
                                          "titulo = @titulo, " +
                                          "subtitulo = @subtitulo, " +
                                          "Valida_desde = @valida_desde, " +
                                          "Valida_hasta = @valida_hasta, " +
                                          "Publicacion_Desde = @publicacion_desde, " +
                                          "Publicacion_Hasta = @publicacion_hasta, " +
                                          "grupo = @grupo, " +
                                          "seo_url = @seo_url, " +
                                          "color_titulos = @color_titulos, " +
                                          "link = @link, " +
                                          "destacado = @destacado, " +
                                          "modificada = 1 " +
                                          "where codigo_promocion = @codigo_promocion;");
                ((SqlParameter)com.Parameters.Add("@codigo_promocion", SqlDbType.Int)).Value = codigo_promocion;
                ((SqlParameter)com.Parameters.Add("@codigo_clasificacion", SqlDbType.Int)).Value = codigo_clasificacion;
                ((SqlParameter)com.Parameters.Add("@codigo_subclasificacion", SqlDbType.Int)).Value = codigo_subclasificacion;                
                ((SqlParameter)com.Parameters.Add("@orden", SqlDbType.Int)).Value = orden;
                ((SqlParameter)com.Parameters.Add("@titulo", SqlDbType.VarChar)).Value = titulo;
                ((SqlParameter)com.Parameters.Add("@subtitulo", SqlDbType.VarChar)).Value = subtitulo;
                ((SqlParameter)com.Parameters.Add("@valida_desde", SqlDbType.Date)).Value = valida_desde;
                ((SqlParameter)com.Parameters.Add("@valida_hasta", SqlDbType.Date)).Value = valida_hasta;
                ((SqlParameter)com.Parameters.Add("@publicacion_desde", SqlDbType.Date)).Value = publicacion_desde;
                ((SqlParameter)com.Parameters.Add("@publicacion_hasta", SqlDbType.Date)).Value = publicacion_hasta;
                ((SqlParameter)com.Parameters.Add("@grupo", SqlDbType.VarChar)).Value = grupo;
                ((SqlParameter)com.Parameters.Add("@seo_url", SqlDbType.VarChar)).Value = seo_url;
                ((SqlParameter)com.Parameters.Add("@color_titulos", SqlDbType.VarChar)).Value = color_titulos;
                ((SqlParameter)com.Parameters.Add("@link", SqlDbType.Text)).Value = link;
                ((SqlParameter)com.Parameters.Add("@destacado", SqlDbType.Bit)).Value = destacado;

                com.ExecuteNonQuery();

                base.commitBaseTransaction();

            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Acualiza datos de una promocion de tipo viaje
        /// </summary>
        public void actualizarPromocion(
            int codigo_promocion, int codigo_clasificacion, int codigo_subclasificacion, int orden,
            string titulo, string subtitulo, DateTime valida_desde, DateTime valida_hasta, string grupo, string seo_url,
            string color_titulos, bool destacado, string link, DateTime publicacion_desde, DateTime publicacion_hasta,
            string operador, int codigo_asesor)
        {
            try
            {

                actualizarOrdenPromociones(orden);

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("update pro_promocion set " +
                                          "codigo_clasificacion = @codigo_clasificacion, " +
                                          "codigo_subclasificacion = @codigo_subclasificacion, " +
                                          "orden = @orden, " +
                                          "titulo = @titulo, " +
                                          "subtitulo = @subtitulo, " +
                                          "Valida_desde = @valida_desde, " +
                                          "Valida_hasta = @valida_hasta, " +
                                          "Publicacion_Desde = @publicacion_desde, " +
                                          "Publicacion_Hasta = @publicacion_hasta, " +
                                          "grupo = @grupo, " +
                                          "seo_url = @seo_url, " +
                                          "color_titulos = @color_titulos, " +
                                          "link = @link, " +
                                          "destacado = @destacado, " +
                                          "modificada = 1 " +
                                          "where codigo_promocion = @codigo_promocion;");
                ((SqlParameter)com.Parameters.Add("@codigo_promocion", SqlDbType.Int)).Value = codigo_promocion;
                ((SqlParameter)com.Parameters.Add("@codigo_clasificacion", SqlDbType.Int)).Value = codigo_clasificacion;
                ((SqlParameter)com.Parameters.Add("@codigo_subclasificacion", SqlDbType.Int)).Value = codigo_subclasificacion;
                ((SqlParameter)com.Parameters.Add("@orden", SqlDbType.Int)).Value = orden;
                ((SqlParameter)com.Parameters.Add("@titulo", SqlDbType.VarChar)).Value = titulo;
                ((SqlParameter)com.Parameters.Add("@subtitulo", SqlDbType.VarChar)).Value = subtitulo;
                ((SqlParameter)com.Parameters.Add("@valida_desde", SqlDbType.Date)).Value = valida_desde;
                ((SqlParameter)com.Parameters.Add("@valida_hasta", SqlDbType.Date)).Value = valida_hasta;
                ((SqlParameter)com.Parameters.Add("@publicacion_desde", SqlDbType.Date)).Value = publicacion_desde;
                ((SqlParameter)com.Parameters.Add("@publicacion_hasta", SqlDbType.Date)).Value = publicacion_hasta;
                ((SqlParameter)com.Parameters.Add("@grupo", SqlDbType.VarChar)).Value = grupo;
                ((SqlParameter)com.Parameters.Add("@seo_url", SqlDbType.VarChar)).Value = seo_url;
                ((SqlParameter)com.Parameters.Add("@color_titulos", SqlDbType.VarChar)).Value = color_titulos;
                ((SqlParameter)com.Parameters.Add("@link", SqlDbType.Text)).Value = link;
                ((SqlParameter)com.Parameters.Add("@destacado", SqlDbType.Bit)).Value = destacado;

                com.ExecuteNonQuery();

                com.Parameters.Clear();
                com = base.getBaseCommand("update pro_promocion_viaje set " +
                                          "codigo_asesor = @codigo_asesor, " +
                                          "operador = @operador " +
                                          "where codigo_promocion = @codigo_promocion;");
                ((SqlParameter)com.Parameters.Add("@codigo_promocion", SqlDbType.Int)).Value = codigo_promocion;
                ((SqlParameter)com.Parameters.Add("@operador", SqlDbType.VarChar)).Value = operador;
                ((SqlParameter)com.Parameters.Add("@codigo_asesor", SqlDbType.Int)).Value = codigo_asesor;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();

            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Acualiza datos de tarifa de una promocion
        /// </summary>
        public void actualizarPromocion(
            int codigo_promocion, string tarifa_moneda, float tarifa, string tipo_tarifa)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("update pro_promocion set " +
                                          "Tarifa_Consolidada_Moneda = @tarifa_moneda, " +
                                          "Tarifa_Consolidada = @tarifa, " +
                                          "Tarifa_Consolidada_Tipo = @tipo_tarifa " +
                                          "where codigo_promocion = @codigo_promocion;");
                ((SqlParameter)com.Parameters.Add("@codigo_promocion", SqlDbType.Int)).Value = codigo_promocion;
                ((SqlParameter)com.Parameters.Add("@tarifa_moneda", SqlDbType.VarChar)).Value = tarifa_moneda;
                ((SqlParameter)com.Parameters.Add("@tarifa", SqlDbType.Float)).Value = tarifa;
                ((SqlParameter)com.Parameters.Add("@tipo_tarifa", SqlDbType.VarChar)).Value = tipo_tarifa;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();

            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Acualiza los ordenes entre las promociones cuando se coloca un orden existente
        /// </summary>
        public void actualizarOrdenPromociones(int orden)
        {
            try
            {
                if (orden < 0) return ;
                int valorOrdenFuturo = orden+1;
                int ordenActual      = orden;
                /*String getOrden = "SELECT rowId as codigo FROM int_seq where i = @orden;";*/
                

                /*String update = "UPDATE int_seq " +
                                "SET i = i + 1 " +
                                "WHERE rowId IN ";*/
                

                /*String updateFirst = "UPDATE int_seq " +
                                    "SET i = i + 1 " +
                                    "WHERE i = @orden;";*/
                String getOrden = "SELECT orden as codigo FROM Pro_Promocion where orden = @orden;";
                String update = "UPDATE Pro_Promocion " +
                                "SET orden = orden + 1 " +
                                "WHERE orden IN ";
                String updateFirst = "UPDATE Pro_Promocion " +
                                    "SET orden = orden + 1 " +
                                    "WHERE orden = @orden;";
                SqlDataAdapter ad;
                DataSet ds;
                Boolean existenOrdenes = true, pimeraVez = true;
                String codigosConcatenados = "";
                DataTable codigos_promo = new DataTable();
                base.beginBaseTransaction();

                while (existenOrdenes)
                {
                    if (pimeraVez)
                    {
                        pimeraVez = false;
                        ad = this.getBaseDataAdapter(getOrden);
                        ((SqlParameter)ad.SelectCommand.Parameters.Add("@orden", SqlDbType.Int)).Value = valorOrdenFuturo;
                        ds = new DataSet();
                        ad.Fill(ds, "Tabla");
                        codigos_promo = ds.Tables[0];
                        codigosConcatenados = "";
                        foreach (DataRow row in codigos_promo.Rows)
                        {
                            codigosConcatenados += row["codigo"] + ",";
                        }
                        
                        SqlCommand com;
                        com = base.getBaseCommand(updateFirst);
                        ((SqlParameter)com.Parameters.Add("@orden", SqlDbType.Int)).Value = ordenActual;
                        com.ExecuteNonQuery();
                        System.Diagnostics.Debug.WriteLine(codigosConcatenados);
                        
                    }
                    else {
                        //calculamos los ordenes antes de actualizar
                        ad = this.getBaseDataAdapter(getOrden);
                        ((SqlParameter)ad.SelectCommand.Parameters.Add("@orden", SqlDbType.Int)).Value = valorOrdenFuturo;
                        ds = new DataSet();
                        ad.Fill(ds, "Tabla");

                        SqlCommand com;
                        com = base.getBaseCommand(update + "(" + codigosConcatenados + ");");
                        ((SqlParameter)com.Parameters.Add("@orden", SqlDbType.Int)).Value = ordenActual;
                        com.ExecuteNonQuery();
                        
                        codigos_promo = ds.Tables[0];
                        codigosConcatenados = "";
                        foreach (DataRow row in codigos_promo.Rows)
                        {
                            codigosConcatenados += row["codigo"] + ",";
                        }
                        System.Diagnostics.Debug.WriteLine(codigosConcatenados);
                    }

                    //si hay algun codigo, verifica que sea mayor que 1 para dejar el string como se desea (a,a,a,a)
                    //en caso que no hayan mas codigos, ejecuta el query actual y no ejecuta la segunda vuelta
                    if (codigos_promo.Rows.Count > 0)
                    {
                        codigosConcatenados = codigosConcatenados.Remove(codigosConcatenados.Length - 1);
                    }
                    else existenOrdenes = false; 
                    valorOrdenFuturo++;
                    ordenActual++;

                                                       
                }
                

                base.commitBaseTransaction();

            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Acualiza la descripcion HTML de una promocion
        /// </summary>
        public void actualizarPromocion(int codigo_promocion, string descripcion)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("update pro_promocion set " +
                                          "descripcion = @descripcion " +
                                          "where codigo_promocion = @codigo_promocion;");
                ((SqlParameter)com.Parameters.Add("@codigo_promocion", SqlDbType.Int)).Value = codigo_promocion;
                ((SqlParameter)com.Parameters.Add("@descripcion", SqlDbType.VarChar)).Value = descripcion;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();

            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Acualiza datos de una promocion en Produccion
        /// </summary>
        public void actualizarPromocionProd(
            int codigo_promocion, int codigo_clasificacion, int codigo_subclasificacion, int orden,
            string tarifa_moneda, decimal tarifa, string tipo_tarifa, string titulo, string subtitulo,
            DateTime valida_desde, DateTime valida_hasta, bool activa, string grupo, string seo_url, string color_titulos,
            string link, bool destacado, int tipo, DateTime publicacion_desde, DateTime publicacion_hasta)
        {
            try
            {

                actualizarOrdenPromociones(orden);

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("update pro_promocion set " +
                                          "codigo_clasificacion = @codigo_clasificacion, " +
                                          "codigo_subclasificacion = @codigo_subclasificacion, " +
                                          "orden = @orden, " +
                                          "Tarifa_Consolidada_Moneda = @tarifa_moneda, " +
                                          "Tarifa_Consolidada = @tarifa, " +
                                          "Tarifa_Consolidada_Tipo = @tipo_tarifa, " +
                                          "titulo = @titulo, " +
                                          "subtitulo = @subtitulo, " +
                                          "Valida_desde = @valida_desde, " +
                                          "Valida_hasta = @valida_hasta, " +
                                          "Publicacion_Desde = @publicacion_desde, " +
                                          "Publicacion_Hasta = @publicacion_hasta, " +
                                          "Activa = @activa, " +
                                          "grupo = @grupo, " +
                                          "seo_url = @seo_url, " +
                                          "color_titulos = @color_titulos, " +
                                          "link = @link, " +
                                          "destacado = @destacado, " +
                                          "tipo = @tipo " +
                                          "where codigo_promocion = @codigo_promocion;");
                ((SqlParameter)com.Parameters.Add("@codigo_promocion", SqlDbType.Int)).Value = codigo_promocion;
                ((SqlParameter)com.Parameters.Add("@codigo_clasificacion", SqlDbType.Int)).Value = codigo_clasificacion;
                ((SqlParameter)com.Parameters.Add("@codigo_subclasificacion", SqlDbType.Int)).Value = codigo_subclasificacion;
                ((SqlParameter)com.Parameters.Add("@orden", SqlDbType.Int)).Value = orden;
                ((SqlParameter)com.Parameters.Add("@tarifa_moneda", SqlDbType.VarChar)).Value = tarifa_moneda;
                ((SqlParameter)com.Parameters.Add("@tarifa", SqlDbType.Decimal)).Value = tarifa;
                ((SqlParameter)com.Parameters.Add("@tipo_tarifa", SqlDbType.VarChar)).Value = tipo_tarifa;
                ((SqlParameter)com.Parameters.Add("@titulo", SqlDbType.VarChar)).Value = titulo;
                ((SqlParameter)com.Parameters.Add("@subtitulo", SqlDbType.VarChar)).Value = subtitulo;
                ((SqlParameter)com.Parameters.Add("@valida_desde", SqlDbType.Date)).Value = valida_desde;
                ((SqlParameter)com.Parameters.Add("@valida_hasta", SqlDbType.Date)).Value = valida_hasta;
                ((SqlParameter)com.Parameters.Add("@publicacion_desde", SqlDbType.Date)).Value = publicacion_desde;
                ((SqlParameter)com.Parameters.Add("@publicacion_hasta", SqlDbType.Date)).Value = publicacion_hasta;
                ((SqlParameter)com.Parameters.Add("@activa", SqlDbType.Bit)).Value = activa;
                ((SqlParameter)com.Parameters.Add("@grupo", SqlDbType.VarChar)).Value = grupo;
                ((SqlParameter)com.Parameters.Add("@seo_url", SqlDbType.VarChar)).Value = seo_url;
                ((SqlParameter)com.Parameters.Add("@color_titulos", SqlDbType.VarChar)).Value = color_titulos;
                ((SqlParameter)com.Parameters.Add("@link", SqlDbType.Text)).Value = link;
                ((SqlParameter)com.Parameters.Add("@destacado", SqlDbType.Bit)).Value = destacado;
                ((SqlParameter)com.Parameters.Add("@tipo", SqlDbType.Int)).Value = tipo;

                com.ExecuteNonQuery();

                base.commitBaseTransaction();

            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Acualiza datos de una promocion en Produccion de tipo viaje
        /// </summary>
        public void actualizarPromocionProd(
            int codigo_promocion, int codigo_clasificacion, int codigo_subclasificacion, int orden,
            string tarifa_moneda, decimal tarifa, string tipo_tarifa, string titulo, string subtitulo,
            DateTime valida_desde, DateTime valida_hasta, bool activa, string grupo, string seo_url, string color_titulos,
            string link, bool destacado, int tipo, DateTime publicacion_desde, DateTime publicacion_hasta, 
            string operador, int codigo_asesor)
        {
            try
            {

                actualizarOrdenPromociones(orden);

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("update pro_promocion set " +
                                          "codigo_clasificacion = @codigo_clasificacion, " +
                                          "codigo_subclasificacion = @codigo_subclasificacion, " +
                                          "orden = @orden, " +
                                          "Tarifa_Consolidada_Moneda = @tarifa_moneda, " +
                                          "Tarifa_Consolidada = @tarifa, " +
                                          "Tarifa_Consolidada_Tipo = @tipo_tarifa, " +
                                          "titulo = @titulo, " +
                                          "subtitulo = @subtitulo, " +
                                          "Valida_desde = @valida_desde, " +
                                          "Valida_hasta = @valida_hasta, " +
                                          "Publicacion_Desde = @publicacion_desde, " +
                                          "Publicacion_Hasta = @publicacion_hasta, " +
                                          "Activa = @activa, " +
                                          "grupo = @grupo, " +
                                          "seo_url = @seo_url, " +
                                          "color_titulos = @color_titulos, " +
                                          "link = @link, " +
                                          "destacado = @destacado, " +
                                          "tipo = @tipo " +
                                          "where codigo_promocion = @codigo_promocion;");
                ((SqlParameter)com.Parameters.Add("@codigo_promocion", SqlDbType.Int)).Value = codigo_promocion;
                ((SqlParameter)com.Parameters.Add("@codigo_clasificacion", SqlDbType.Int)).Value = codigo_clasificacion;
                ((SqlParameter)com.Parameters.Add("@codigo_subclasificacion", SqlDbType.Int)).Value = codigo_subclasificacion;
                ((SqlParameter)com.Parameters.Add("@orden", SqlDbType.Int)).Value = orden;
                ((SqlParameter)com.Parameters.Add("@tarifa_moneda", SqlDbType.VarChar)).Value = tarifa_moneda;
                ((SqlParameter)com.Parameters.Add("@tarifa", SqlDbType.Decimal)).Value = tarifa;
                ((SqlParameter)com.Parameters.Add("@tipo_tarifa", SqlDbType.VarChar)).Value = tipo_tarifa;
                ((SqlParameter)com.Parameters.Add("@titulo", SqlDbType.VarChar)).Value = titulo;
                ((SqlParameter)com.Parameters.Add("@subtitulo", SqlDbType.VarChar)).Value = subtitulo;
                ((SqlParameter)com.Parameters.Add("@valida_desde", SqlDbType.Date)).Value = valida_desde;
                ((SqlParameter)com.Parameters.Add("@valida_hasta", SqlDbType.Date)).Value = valida_hasta;
                ((SqlParameter)com.Parameters.Add("@publicacion_desde", SqlDbType.Date)).Value = publicacion_desde;
                ((SqlParameter)com.Parameters.Add("@publicacion_hasta", SqlDbType.Date)).Value = publicacion_hasta;
                ((SqlParameter)com.Parameters.Add("@activa", SqlDbType.Bit)).Value = activa;
                ((SqlParameter)com.Parameters.Add("@grupo", SqlDbType.VarChar)).Value = grupo;
                ((SqlParameter)com.Parameters.Add("@seo_url", SqlDbType.VarChar)).Value = seo_url;
                ((SqlParameter)com.Parameters.Add("@color_titulos", SqlDbType.VarChar)).Value = color_titulos;
                ((SqlParameter)com.Parameters.Add("@link", SqlDbType.Text)).Value = link;
                ((SqlParameter)com.Parameters.Add("@destacado", SqlDbType.Bit)).Value = destacado;
                ((SqlParameter)com.Parameters.Add("@tipo", SqlDbType.Int)).Value = tipo;

                com.ExecuteNonQuery();

                com.Parameters.Clear();
                com = base.getBaseCommand("update pro_promocion_viaje set " +
                                          "codigo_asesor = @codigo_asesor, " +
                                          "operador = @operador " +
                                          "where codigo_promocion = @codigo_promocion;");
                ((SqlParameter)com.Parameters.Add("@codigo_promocion", SqlDbType.Int)).Value = codigo_promocion;
                ((SqlParameter)com.Parameters.Add("@operador", SqlDbType.VarChar)).Value = operador;
                ((SqlParameter)com.Parameters.Add("@codigo_asesor", SqlDbType.Int)).Value = codigo_asesor;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();

            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Acualiza status de una promocion (modificada = 1 o modificada = 0)
        /// </summary>
        public void actualizarStatusModificada(int codigo_promocion, int modificada)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("update pro_promocion set " +
                                          "modificada = @modificada " +
                                          "where codigo_promocion = @codigo_promocion;");
                ((SqlParameter)com.Parameters.Add("@codigo_promocion", SqlDbType.Int)).Value = codigo_promocion;
                ((SqlParameter)com.Parameters.Add("@modificada", SqlDbType.Int)).Value = modificada;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();

            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Elimina una promocion
        /// </summary>
        public void eliminarPromocion(int codigo_promocion)
        {
            try
            {
                base.beginBaseTransaction();

                Promocion p = new Promocion(this.getBaseContextForNewInstance(), codigo_promocion);

                SqlCommand com;

                com = base.getBaseCommand();
                com.CommandText = "delete from Pro_Promocion where codigo_promocion = @codigo_promocion;";
                com.Parameters.Add("@codigo_promocion", SqlDbType.Int).Value = codigo_promocion;
                com.ExecuteNonQuery();

                com.CommandText = "delete from Pro_Promocion_Tarifa where codigo_promocion = @codigo_promocion;";
                com.Parameters.Clear();
                com.Parameters.Add("@codigo_promocion", SqlDbType.Int).Value = codigo_promocion;
                com.ExecuteNonQuery();


                com.CommandText = "delete from Pro_Promocion_Texto_Libre where codigo_promocion = @codigo_promocion;";
                com.Parameters.Clear();
                com.Parameters.Add("@codigo_promocion", SqlDbType.Int).Value = codigo_promocion;
                com.ExecuteNonQuery();

                
                if (p.tipo == Tipo_Promocion.Promocion_viaje)
                {
                    com.CommandText = "delete from Pro_Promocion_Viaje where codigo_promocion = @codigo_promocion;";
                    com.Parameters.Clear();
                    com.Parameters.Add("@codigo_promocion", SqlDbType.Int).Value = codigo_promocion;
                    com.ExecuteNonQuery();
                }

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Devuelve la lista de los textos libres de una promocion
        /// </summary>
        public DataTable listarTextosLibres(int codigo_promocion)
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds = new DataSet();

                ad = base.getBaseDataAdapter("select * from Pro_Promocion_Texto_Libre where codigo_promocion = @codigo_promocion order by orden;");
                ((SqlParameter)ad.SelectCommand.Parameters.Add("@codigo_promocion", SqlDbType.Int)).Value = codigo_promocion;
                ad.Fill(ds);

                base.commitBaseTransaction();

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Registra un nuevo texto libre
        /// </summary>
        public void registrarTextoLibre(int codigo_promocion, string titulo, int orden)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("insert into Pro_Promocion_Texto_Libre "
                                         + "(Codigo_Promocion, Titulo_Texto_Libre, Texto_Libre, Orden) values "
                                         + "(@codigo_promocion, @titulo, @texto, @orden);");
                com.Parameters.Add("@codigo_promocion", SqlDbType.Int).Value = codigo_promocion;
                com.Parameters.Add("@titulo", SqlDbType.VarChar).Value = titulo;
                com.Parameters.Add("@texto", SqlDbType.VarChar).Value = "";
                com.Parameters.Add("@orden", SqlDbType.Int).Value = orden;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Registra un nuevo texto libre
        /// </summary>
        public void registrarTextoLibre(int codigo_promocion, string titulo, string texto, int orden)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("insert into Pro_Promocion_Texto_Libre "
                                         + "(Codigo_Promocion, Titulo_Texto_Libre, Texto_Libre, Orden) values "
                                         + "(@codigo_promocion, @titulo, @texto, @orden);");
                com.Parameters.Add("@codigo_promocion", SqlDbType.Int).Value = codigo_promocion;
                com.Parameters.Add("@titulo", SqlDbType.VarChar).Value = titulo;
                com.Parameters.Add("@texto", SqlDbType.VarChar).Value = texto;
                com.Parameters.Add("@orden", SqlDbType.Int).Value = orden;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Elimina un texto libre
        /// </summary>
        public void eliminarTextoLibre(int codigo_promocion, string titulo)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand();
                com.CommandText = "delete from Pro_Promocion_Texto_Libre where codigo_promocion = @codigo_promocion and titulo_texto_libre = @titulo;";
                com.Parameters.Add("@codigo_promocion", SqlDbType.Int).Value = codigo_promocion;
                com.Parameters.Add("@titulo", SqlDbType.VarChar).Value = titulo;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Actualiza titulo de un texto libre
        /// </summary>
        public void actualizarTituloTextoLibre(int codigo_promocion, string titulo_previo, string titulo)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("update Pro_Promocion_Texto_Libre set " +
                                          "titulo_texto_libre = @titulo " +
                                          "where codigo_promocion = @codigo_promocion and titulo_texto_libre = @titulo_previo ;");
                com.Parameters.Add("@codigo_promocion", SqlDbType.Int).Value = codigo_promocion;
                com.Parameters.Add("@titulo", SqlDbType.VarChar).Value = titulo;
                com.Parameters.Add("@titulo_previo", SqlDbType.VarChar).Value = titulo_previo;

                com.ExecuteNonQuery();

                base.commitBaseTransaction();

            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Actualiza el texto de un texto libre
        /// </summary>
        public void actualizarTextoTextoLibre(int codigo_promocion, string titulo, string texto)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("update Pro_Promocion_Texto_Libre set " +
                                          "texto_libre = @texto " +
                                          "where codigo_promocion = @codigo_promocion and titulo_texto_libre = @titulo ;");
                com.Parameters.Add("@codigo_promocion", SqlDbType.Int).Value = codigo_promocion;
                com.Parameters.Add("@titulo", SqlDbType.VarChar).Value = titulo;
                com.Parameters.Add("@texto", SqlDbType.VarChar).Value = texto;

                com.ExecuteNonQuery();

                base.commitBaseTransaction();

            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Actualiza el orden de un texto libre
        /// titulo1: identificador del textoLibre a cambiar orden, orden1: nuevo orden de titulo1
        /// titulo2: identificador del textoLibre a cambiar orden, orden2: nuevo orden de titulo2
        /// </summary>
        public void actualizarOrdenTextoLibre(int codigo_promocion, string titulo1, int orden1, string titulo2, int orden2)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("update Pro_Promocion_Texto_Libre set " +
                                          "orden = @orden2 " +
                                          "where codigo_promocion = @codigo_promocion and titulo_texto_libre = @titulo1 ;");
                com.Parameters.Add("@codigo_promocion", SqlDbType.Int).Value = codigo_promocion;
                com.Parameters.Add("@titulo1", SqlDbType.VarChar).Value = titulo1;
                com.Parameters.Add("@orden2", SqlDbType.Int).Value = orden2;

                com.ExecuteNonQuery();

                com = base.getBaseCommand("update Pro_Promocion_Texto_Libre set " +
                                          "orden = @orden1 " +
                                          "where codigo_promocion = @codigo_promocion and titulo_texto_libre = @titulo2 ;");
                com.Parameters.Add("@codigo_promocion", SqlDbType.Int).Value = codigo_promocion;
                com.Parameters.Add("@titulo2", SqlDbType.VarChar).Value = titulo2;
                com.Parameters.Add("@orden1", SqlDbType.Int).Value = orden1;

                com.ExecuteNonQuery();

                base.commitBaseTransaction();

            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Selecciona el ultimo orden de un texto libre devuelve su valor.
        /// </summary>
        public int seleccionarUltimoOrdenTextoLibre(int codigo_promocion)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com = this.getBaseCommand();
                int temp;

                com.CommandText = "SELECT TOP 1 Orden FROM Pro_Promocion_Texto_Libre where codigo_promocion = @codigo_promocion order by orden DESC;";
                com.Parameters.Add("@codigo_promocion", SqlDbType.Int).Value = codigo_promocion;
                temp = ((int)com.ExecuteScalar());

                base.commitBaseTransaction();

                return temp;
            }
            catch (Exception ex)
            {               
                base.rollbackBaseTransaction();
                return 1;
                throw ex;
            }
        }

        /// <summary>
        /// Selecciona el titulo de un texto libre.
        /// </summary>
        public string seleccionarTituloTextoLibre(int codigo_promocion, int orden)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com = this.getBaseCommand();
                string temp;

                com.CommandText = "SELECT TOP 1 titulo_texto_libre FROM Pro_Promocion_Texto_Libre where codigo_promocion = @codigo_promocion and orden = @orden order by orden DESC;";
                com.Parameters.Add("@codigo_promocion", SqlDbType.Int).Value = codigo_promocion;
                com.Parameters.Add("@orden", SqlDbType.Int).Value = orden;
                temp = ((string)com.ExecuteScalar());

                base.commitBaseTransaction();

                return temp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


    }
}
