﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using passarini.web.business.transacciones;


namespace PromoCotizaciones.business
{
    public class Resultados
    {

        public int codigo_promocion;
        public Exception error;
        public string mensaje;


        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public Resultados()
        {          
        }

        public Resultados(int codigo, Exception e, string msg){
            this.codigo_promocion = codigo;
            this.error = e;
            this.mensaje = msg;
        }
                
        
        

    }
}
