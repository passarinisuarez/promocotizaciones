﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using passarini.web.business.transacciones;

namespace PromoCotizaciones.business
{
    public class Clasificacion
    {

        public List<int> clasificaciones = new List<int>();

        public const int bannerSup = 1;
        public const int precios = 2;
        public const int mapa = 3;
        public const int cruceros = 4;
        public const int especiales = 5;
        public const int servicios = 6;
        public const int hotelescorporativos = 7;
        public const int flyers = 8;


        public Clasificacion()
        {
            clasificaciones.Add(1); clasificaciones.Add(2); clasificaciones.Add(3); clasificaciones.Add(4); clasificaciones.Add(5); clasificaciones.Add(6); clasificaciones.Add(7); clasificaciones.Add(8);
        }

        public static string valorClasificacion(int valor) { 
            switch (valor){
                case bannerSup:
                    return "Solo en Banner Destacado";
                case precios:
                    return "Nacionales";
                case mapa:
                    return "Internacionales";
                case cruceros:
                    return "Cruceros";
                case especiales:
                    return "Especiales";
                case servicios:
                    return "Servicios";
                case hotelescorporativos:
                    return "Hoteles Corporativos";
                case flyers:
                    return "Promociones";
                default:
                    return "";
            }
        } 

    }
}
