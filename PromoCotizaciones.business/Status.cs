﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using passarini.web.business.transacciones;

namespace PromoCotizaciones.business
{
    public class Status : Base
    {

        public const char statusSolicitada = 'S';
        public const char statusCotizada = 'C';
        public const char statusAnulada = 'A';
        public const char statusCompraRealizada = 'R';

        public Status(BaseContext baseContext)
            : base(baseContext)
        {
        
        }

        public static string valorStatus(char valor)
        { 
            switch (valor){
                case statusSolicitada:
                    return "Solicitada";
                case statusCotizada:
                    return "Cotizada";
                case statusAnulada:
                    return "Anulada";
                case statusCompraRealizada:
                    return "Compra Realizada";
                default:
                    return "";
            }
        } 

    }
}
