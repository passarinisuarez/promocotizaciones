﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using passarini.web.business.transacciones;


namespace PromoCotizaciones.business
{
    public class Cotizaciones: Base
	{

        /// <summary>
        /// Constructor de la clase. Recibe un contexto de conexión a base de datos.
        /// </summary>
        public Cotizaciones(BaseContext baseContext) : base(baseContext)
		{
        }

        /// <summary>
        /// Devuelve la lista de las cotitzaciones
        /// </summary>
        public DataTable listarCotizaciones()
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds = new DataSet();

                ad = base.getBaseDataAdapter("select * from Cot_Cotizacion order by codigo_cotizacion DESC;");
                ad.Fill(ds);

                base.commitBaseTransaction();

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Devuelve la lista de las cotitzaciones de un asesor especifico
        /// </summary>
        public DataTable listarCotizaciones(int codigo_asesor)
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds = new DataSet();

                ad = base.getBaseDataAdapter("select * from Cot_Cotizacion where codigo_asesor = '"+codigo_asesor+"';");
                ad.Fill(ds);

                base.commitBaseTransaction();

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Registra una cotizacion
        /// </summary>
        public int registrarCotizacion(int codigo_promo, string nombre, string telefono, string email, string ip,
                                       string texto_solicitud, char via, char status,
                                       int num_adultos, int num_ninos, int num_infantes, int codigo_asesor)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("insert into Cot_Cotizacion (Codigo_Promocion, Nombre, Telefono, Email, IP, Texto_Solicitud, Via, Status, Numero_Adultos, Numero_Ninos, Numero_Infantes, Codigo_Asesor) values "
                                         + "(@codigo_promo, @Nombre, @telefono, @Email, @ip, @texto_solicitud, @via, @status, @num_adultos, @num_ninos, @num_infantes, @codigo_asesor);");
                com.Parameters.Add("@codigo_promo", SqlDbType.Int).Value = codigo_promo < 0 ? DBNull.Value : (Object)codigo_promo;
                com.Parameters.Add("@nombre", SqlDbType.VarChar).Value = nombre;
                com.Parameters.Add("@telefono", SqlDbType.VarChar).Value = telefono;
                com.Parameters.Add("@email", SqlDbType.VarChar).Value = email;
                com.Parameters.Add("@ip", SqlDbType.VarChar).Value = ip;
                com.Parameters.Add("@texto_solicitud", SqlDbType.Text).Value = texto_solicitud;
                com.Parameters.Add("@via", SqlDbType.Char).Value = via;
                com.Parameters.Add("@status", SqlDbType.Char).Value = status;
                com.Parameters.Add("@num_adultos", SqlDbType.Int).Value = num_adultos;
                com.Parameters.Add("@num_ninos", SqlDbType.Int).Value = num_ninos;
                com.Parameters.Add("@num_infantes", SqlDbType.Int).Value = num_infantes;
                com.Parameters.Add("@codigo_asesor", SqlDbType.Int).Value = codigo_asesor;
                com.ExecuteNonQuery();

                SqlDataReader sqldr;
                int temp;

                com.CommandText = "select @@identity;";
                com.Parameters.Clear();
                sqldr = com.ExecuteReader();
                sqldr.Read();
                temp = int.Parse(sqldr[0].ToString());
                sqldr.Close();

                base.commitBaseTransaction();
                return temp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Actualiza una cotizacion
        /// </summary>
        public void actualizarCotizacion(int codigo_cotizacion, int codigo_promo, string nombre, string telefono, 
                                        string email, string ip, string texto_solicitud, char via, char status, 
                                        int num_adultos, int num_ninos, int num_infantes, int codigo_asesor)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("update Cot_Cotizacion set "+
                                          "codigo_promocion = @codigo_promo, nombre = @nombre, " +
                                          "telefono = @telefono, email = @email, ip = @ip, texto_solicitud = @texto_solicitud, "+
                                          "via = @via, status = @status, numero_adultos = @num_adultos, "+
                                          "numero_ninos = @num_ninos, numero_infantes = @num_infantes, codigo_asesor = @codigo_asesor "+
                                          "where codigo_cotizacion = @codigo_cotizacion ;");
                com.Parameters.Add("@codigo_cotizacion", SqlDbType.Int).Value = codigo_cotizacion;
                com.Parameters.Add("@codigo_promo", SqlDbType.Int).Value = codigo_promo < 0 ? DBNull.Value : (Object)codigo_promo;
                com.Parameters.Add("@nombre", SqlDbType.VarChar).Value = nombre;
                com.Parameters.Add("@telefono", SqlDbType.VarChar).Value = telefono;
                com.Parameters.Add("@email", SqlDbType.VarChar).Value = email;
                com.Parameters.Add("@ip", SqlDbType.VarChar).Value = ip;
                com.Parameters.Add("@texto_solicitud", SqlDbType.Text).Value = texto_solicitud;
                com.Parameters.Add("@via", SqlDbType.Char).Value = via;
                com.Parameters.Add("@status", SqlDbType.Char).Value = status;
                com.Parameters.Add("@num_adultos", SqlDbType.Int).Value = num_adultos;
                com.Parameters.Add("@num_ninos", SqlDbType.Int).Value = num_ninos;
                com.Parameters.Add("@num_infantes", SqlDbType.Int).Value = num_infantes;
                com.Parameters.Add("@codigo_asesor", SqlDbType.Int).Value = codigo_asesor;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
                
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }



        /// <summary>
        /// Actualizar el status de una cotizacion
        /// </summary>
        public void cambiarStatusCotizacion(int codigo_cotizacion, char status)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("update Cot_Cotizacion set " +
                                          "status = @status where codigo_cotizacion = @codigo_cotizacion;");
                com.Parameters.Add("@codigo_cotizacion", SqlDbType.Int).Value = codigo_cotizacion;
                com.Parameters.Add("@status", SqlDbType.Char).Value = status;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();

            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Actualizar el status de una cotizacion
        /// </summary>
        public void cambiarAsesorCotizacion(int codigo_cotizacion, int codigo_asesor)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("update Cot_Cotizacion set " +
                                          "codigo_asesor = @codigo_asesor where codigo_cotizacion = @codigo_cotizacion;");
                com.Parameters.Add("@codigo_cotizacion", SqlDbType.Int).Value = codigo_cotizacion;
                com.Parameters.Add("@codigo_asesor", SqlDbType.Int).Value = codigo_asesor;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();

            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Elimina una cotizacion
        /// </summary>
        public void eliminarCotizacion(int codigo_cotizacion)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand();
                com.CommandText = "delete from Cot_Cotizacion where codigo_cotizacion = @codigo_cotizacion;";
                com.Parameters.Add("@codigo_cotizacion", SqlDbType.VarChar).Value = codigo_cotizacion;
                if (com.ExecuteNonQuery() == 0)
                {
                    throw new Exception("El usuario a eliminar no existe, o está siendo referenciado en otras tablas.");
                }
                com.CommandText = "delete from Cot_Cotizacion where codigo_cotizacion = @codigo_cotizacion;";
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Lista las tarifas detalladas de la Cotización dada
        /// </summary>
        public DataTable listarTarifa(int codigoCotizacion)
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds;

                ad = this.getBaseDataAdapter("select * from Cot_Cotizacion_Detalle_Tarifa where Codigo_Cotizacion = @codigo_cotizacion;");
                ((SqlParameter)ad.SelectCommand.Parameters.Add("@codigo_cotizacion", SqlDbType.Int)).Value = codigoCotizacion;
                ds = new DataSet();
                ad.Fill(ds, "Tabla");

                base.commitBaseTransaction();

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Registra una tarifa de cotizacion
        /// </summary>
        public void registrarTarifaCotizacion(int codigo_cotizacion, string concepto, string moneda, float monto, int orden)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("insert into Cot_Cotizacion_Detalle_Tarifa "
                                         + "(Codigo_Cotizacion, Concepto, Moneda, Monto, Orden_Tarifa) values "
                                         + "(@codigo_cotizacion, @concepto, @moneda, @monto, @orden);");
                com.Parameters.Add("@codigo_cotizacion", SqlDbType.Int).Value = codigo_cotizacion;
                com.Parameters.Add("@concepto", SqlDbType.VarChar).Value = concepto;
                com.Parameters.Add("@moneda", SqlDbType.VarChar).Value = moneda;
                com.Parameters.Add("@monto", SqlDbType.Float).Value = monto;
                com.Parameters.Add("@orden", SqlDbType.Int).Value = orden;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Elimina una tarifa
        /// </summary>
        public void eliminarTarifa(int codigo_cotizacion, int orden)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand();
                com.CommandText = "delete from Cot_Cotizacion_Detalle_Tarifa where codigo_cotizacion = @codigo_cotizacion and orden_tarifa = @orden;";
                com.Parameters.Add("@codigo_cotizacion", SqlDbType.Int).Value = codigo_cotizacion;
                com.Parameters.Add("@orden", SqlDbType.Int).Value = orden;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Selecciona la ultima tarifa y devuelve su codigo.
        /// </summary>
        public int seleccionarUltimaTarifa(int codigo_cotizacion)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com = this.getBaseCommand();
                int temp;

                com.CommandText = "SELECT TOP 1 Orden_Tarifa FROM Cot_Cotizacion_Detalle_Tarifa where codigo_cotizacion = @codigo_cotizacion order by Orden_Tarifa DESC;";
                com.Parameters.Add("@codigo_cotizacion", SqlDbType.Int).Value = codigo_cotizacion;
                temp = ((int)com.ExecuteScalar());

                base.commitBaseTransaction();

                return temp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Actualiza datos de una tarifa
        /// </summary>
        public void actualizarTarifa(int codigo_cotizacion, string concepto, string moneda, float monto, int orden)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("update Cot_Cotizacion_Detalle_Tarifa set " +
                                          "concepto = @concepto, " +
                                          "moneda = @moneda, " +
                                          "monto = @monto " +
                                          "where codigo_cotizacion = @codigo_cotizacion and Orden_Tarifa = @orden;");
                com.Parameters.Add("@codigo_cotizacion", SqlDbType.Int).Value = codigo_cotizacion;
                com.Parameters.Add("@concepto", SqlDbType.VarChar).Value = concepto;
                com.Parameters.Add("@moneda", SqlDbType.VarChar).Value = moneda;
                com.Parameters.Add("@monto", SqlDbType.Float).Value = monto;
                com.Parameters.Add("@orden", SqlDbType.Int).Value = orden;

                com.ExecuteNonQuery();

                base.commitBaseTransaction();

            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Registra la fecha y el status de la cotizacion
        /// </summary>
        public void registrarFechaStatus(int codigo_cotizacion, DateTime fecha_hora, char status, int codigo_asesor)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("insert into Cot_Cotizacion_Status "
                                         + "(Codigo_Cotizacion, Fecha_Hora, Status, Codigo_Asesor) values "
                                         + "(@codigo_cotizacion, @fecha_hora, @status, @codigo_asesor);");
                com.Parameters.Add("@codigo_cotizacion", SqlDbType.Int).Value = codigo_cotizacion;
                com.Parameters.Add("@fecha_hora", SqlDbType.DateTime).Value = fecha_hora;
                com.Parameters.Add("@status", SqlDbType.Char).Value = status;
                com.Parameters.Add("@codigo_asesor", SqlDbType.Int).Value = codigo_asesor;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Obtiene la fecha del ultimo status de la cotizacion
        /// </summary>
        public string obtenerUltimaFecha(int codigo_cotizacion)
        {
            try
            {

                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds;

                ad = this.getBaseDataAdapter("SELECT TOP 1 [Fecha_Hora] "                                          
                                          +" FROM [passarini.web.local].[dbo].[Cot_Cotizacion_Status] "
                                          +" WHERE Codigo_Cotizacion = @codigo_cotizacion "
                                          +" order by Fecha_Hora DESC ");
                ((SqlParameter)ad.SelectCommand.Parameters.Add("@codigo_cotizacion", SqlDbType.Int)).Value = codigo_cotizacion;
                ds = new DataSet();
                ad.Fill(ds, "Tabla");

                base.commitBaseTransaction();

                return ds.Tables[0].Rows[0]["Fecha_Hora"].ToString();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Devuelve la lista de los textos libres de una cotitzacion
        /// </summary>
        public DataTable listarTextosLibres(int codigo_cotizacion)
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds = new DataSet();

                ad = base.getBaseDataAdapter("select * from Cot_Cotizacion_Texto_Libre where codigo_cotizacion = @codigo_cotizacion order by Titulo_Texto_Libre DESC;");
                ((SqlParameter)ad.SelectCommand.Parameters.Add("@codigo_cotizacion", SqlDbType.Int)).Value = codigo_cotizacion;
                ad.Fill(ds);

                base.commitBaseTransaction();

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Registra un nuevo texto libre
        /// </summary>
        public void registrarTextoLibre(int codigo_cotizacion, string titulo)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("insert into Cot_Cotizacion_Texto_Libre "
                                         + "(Codigo_Cotizacion, Titulo_Texto_Libre, Texto_Libre) values "
                                         + "(@codigo_cotizacion, @titulo, @texto);");
                com.Parameters.Add("@codigo_cotizacion", SqlDbType.Int).Value = codigo_cotizacion;
                com.Parameters.Add("@titulo", SqlDbType.VarChar).Value = titulo;
                com.Parameters.Add("@texto", SqlDbType.VarChar).Value = "";
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Elimina un texto libre
        /// </summary>
        public void eliminarTextoLibre(int codigo_cotizacion, string titulo)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand();
                com.CommandText = "delete from Cot_Cotizacion_Texto_Libre where codigo_cotizacion = @codigo_cotizacion and titulo_texto_libre = @titulo;";
                com.Parameters.Add("@codigo_cotizacion", SqlDbType.Int).Value = codigo_cotizacion;
                com.Parameters.Add("@titulo", SqlDbType.VarChar).Value = titulo;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Actualiza titulo de un texto libre
        /// </summary>
        public void actualizarTituloTextoLibre(int codigo_cotizacion, string titulo_previo, string titulo)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("update Cot_Cotizacion_Texto_Libre set " +
                                          "titulo_texto_libre = @titulo " +
                                          "where codigo_cotizacion = @codigo_cotizacion and titulo_texto_libre = @titulo_previo ;");
                com.Parameters.Add("@codigo_cotizacion", SqlDbType.Int).Value = codigo_cotizacion;
                com.Parameters.Add("@titulo", SqlDbType.VarChar).Value = titulo;
                com.Parameters.Add("@titulo_previo", SqlDbType.VarChar).Value = titulo_previo;

                com.ExecuteNonQuery();

                base.commitBaseTransaction();

            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Actualiza el texto de un texto libre
        /// </summary>
        public void actualizarTextoTextoLibre(int codigo_cotizacion,string titulo, string texto)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("update Cot_Cotizacion_Texto_Libre set " +
                                          "texto_libre = @texto " +
                                          "where codigo_cotizacion = @codigo_cotizacion and titulo_texto_libre = @titulo ;");
                com.Parameters.Add("@codigo_cotizacion", SqlDbType.Int).Value = codigo_cotizacion;
                com.Parameters.Add("@titulo", SqlDbType.VarChar).Value = titulo;
                com.Parameters.Add("@texto", SqlDbType.VarChar).Value = texto;

                com.ExecuteNonQuery();

                base.commitBaseTransaction();

            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

    }
}
