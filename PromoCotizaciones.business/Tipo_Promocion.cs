﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PromoCotizaciones.business
{
    /// <summary>
    /// Maneja los tipos de promociones existentes. 
    /// Metodos marcados con ** son utilizados SOLO en el sitio web de passarini.
    /// </summary>
    public class Tipo_Promocion
    {

        public List<int> tipos_promos = new List<int>();
        public const int Promocion_general = 1;
        public const int Promocion_viaje = 2;        

        public Tipo_Promocion()
        {
            tipos_promos.Add(1); tipos_promos.Add(2);
        }

        public static string StringTipoPromo(int valor) 
        { 
            switch (valor)
            {
                case Promocion_general:
                    return "General";
                case Promocion_viaje:
                    return "Viaje";
                default:
                    return "";
            }
        }

        public static bool esPromoViaje(int valor)
        {
            return valor == Promocion_viaje;
        }

        public static bool esPromoGeneral(int valor)
        {
            return valor == Promocion_general;
        }

    }
}
