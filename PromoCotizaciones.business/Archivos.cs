﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using passarini.web.business.transacciones;


namespace PromoCotizaciones.business
{
    public class Archivos
    {

        private string path;


        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public Archivos()
        {          
        }

        public Archivos(string path){
            this.path = path;
        }
                
        public void Eliminar()
        {
            if (File.Exists(this.path))
            {
                File.Delete(this.path);
            }
        }

        public void EliminarGaleria()
        {

            DirectoryInfo directory = new DirectoryInfo(this.path);
            FileInfo[] files = null;
            try { files = directory.GetFiles("*.*"); }
            catch { }

            if (files != null && files.Length > 0)
            {
                foreach (var fot in files)
                {
                    File.Delete(this.path+"/"+fot.Name);
                }
            }
        } 
        

    }
}
