﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using passarini.web.business.transacciones;

namespace PromoCotizaciones.business
{
    public class Destino : Base
    {

        public List<int> destinos = new List<int>();
        public List<int> destinosInternac = new List<int>();
        public List<int> destinosCruceros = new List<int>();
        public List<int> destinosEspeciales = new List<int>();
        public List<int> destinosServicios = new List<int>();
        public List<int> destinosHotelesCorporativos = new List<int>(); 

        //tipos de Destinos Nacionales
        public const int Los_Roques = 1;
        public const int Merida = 2;
        public const int Margarita = 3;
        public const int Canaima = 4;
        public const int Pto_La_Cruz = 5;
        public const int Otros = 6;

        //tipos de Destinos Internacionales
        public const int America_USA_Canada = 1;
        public const int Centro_America = 2;
        public const int Islas_Caribe = 3;
        public const int Sur_America = 4;
        public const int Eur = 5;
        public const int Africa = 6;
        public const int Asia = 7;
        public const int Oceania = 8;

        //tipos de Destinos Cruceros
        public const int Caribe = 1;
        public const int MarMediterraneo = 2;
        public const int Baltico = 3;
        public const int EuropaNorte = 4;
        public const int TransAmerica = 5;
        public const int TransEuropa = 6;
        public const int Sur_America_Crucero = 7;

        //tipos de Destinos Especiales
        public const int CampVerano = 1;
        public const int LunaMiel = 2;
        public const int CaminoReligioso = 3;
        public const int ConciertoEvento = 4;
        public const int ViajePorElMundo = 5;
        public const int PuentesYFeriados = 6;
        public const int Tours = 7;
        public const int Lopcymat = 8;

        //tipos de Destinos Servicios
        public const int AlquilerCarros = 1;
        public const int SeguroViaje = 2;

        //tipos de Destinos Hoteles Corporativos
        public const int Nacionales = 1;
        public const int Internacionales = 2;

        public Destino(BaseContext baseContext)
            : base(baseContext)
        {
            destinos.Add(1); destinos.Add(2); destinos.Add(3); destinos.Add(4); destinos.Add(5); destinos.Add(6);
            destinosInternac.Add(1); destinosInternac.Add(2); destinosInternac.Add(3); destinosInternac.Add(4); destinosInternac.Add(5); destinosInternac.Add(6); destinosInternac.Add(7); destinosInternac.Add(8);
            destinosCruceros.Add(1); destinosCruceros.Add(2); destinosCruceros.Add(3); destinosCruceros.Add(4); destinosCruceros.Add(5); destinosCruceros.Add(6); destinosCruceros.Add(7);
            destinosEspeciales.Add(1); destinosEspeciales.Add(2); destinosEspeciales.Add(3); destinosEspeciales.Add(4); destinosEspeciales.Add(5); destinosEspeciales.Add(6); destinosEspeciales.Add(7); destinosEspeciales.Add(8);
            destinosServicios.Add(1); destinosServicios.Add(2);
            destinosHotelesCorporativos.Add(1); destinosHotelesCorporativos.Add(2); 

        }

        public static string valorDestino(int valor) { 
            switch (valor){
                case Los_Roques:
                    return "Los Roques";
                case Merida:
                    return "Mérida";
                case Margarita:
                    return "Margarita";
                case Canaima:
                    return "Canaima";
                case Pto_La_Cruz:
                    return "Puerto la Cruz";
                case Otros:
                    return "Otros";
                default:
                    return "";
            }
        }

        public static string valorDestinoInternacionales(int valor)
        {
            switch (valor)
            {
                case America_USA_Canada:
                    return "América USA y Canadá";
                case Centro_America:
                    return "Centroamérica";
                case Islas_Caribe:
                    return "Islas del Caribe";
                case Sur_America:
                    return "Sur América";
                case Eur:
                    return "Europa";
                case Africa:
                    return "África";
                case Asia:
                    return "Asia";
                case Oceania:
                    return "Oceanía";
                default:
                    return "";
            }
        }

        public static string valorCruceros(int valor)
        {
            switch (valor)
            {
                case Caribe:
                    return "Caribe";
                case MarMediterraneo:
                    return "Mediteráneo";
                case Baltico:
                    return "Baltico";
                case EuropaNorte:
                    return "Europa del Norte";
                case TransAmerica:
                    return "Transatlántico desde America";
                case TransEuropa:
                    return "Transatlántico desde Europa";
                case Sur_America_Crucero:
                    return "Sur América";
                default:
                    return "";
            }
        }

        public static string valorEspeciales(int valor)
        {            
            switch (valor)
            {
                case CampVerano:
                    return "Campamentos de Verano";
                case LunaMiel:
                    return "Luna de Miel";
                case CaminoReligioso:
                    return "Caminos Religiosos";
                case ConciertoEvento:
                    return "Conciertos y Eventos";
                case ViajePorElMundo:
                    return "Viaja por El Mundo";
                case PuentesYFeriados:
                    return "Puentes y Feriados";
                case Tours:
                    return "Tours";
                case Lopcymat:
                    return "LOPCYMAT";
                default:
                    return "";
            }
        }

        public static string valorServicios(int valor)
        {
            switch (valor)
            {
                case AlquilerCarros:
                    return "Alquiler de Carros";
                case SeguroViaje:
                    return "Seguro de Viajes";
                default:
                    return "";
            }
        }

        public static string valorHotelesCorporativos(int valor)
        {
            switch (valor)
            {
                case Nacionales:
                    return "Nacionales";
                case Internacionales:
                    return "Internacionales";
                default:
                    return "";
            }
        }

    }
}
