﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using passarini.web.business.transacciones;

namespace PromoCotizaciones.business
{
    public class Tarifa : Base
    {

        public List<int> tarifas = new List<int>();
        public List<int> tarifas_monedas = new List<int>(); 
        //tipos de tarifa
        public const int P = 1;
        public const int D = 2;
        public const int L = 3;
        public const int V = 4;

        //tipos tarifa moneda
        public const int DPN = 1;
        public const int EUROS = 2;
        public const int DOLAR = 4;
        public const int BSF = 5;
        public const int BSS = 6;

        public Tarifa(BaseContext baseContext)
            : base(baseContext)
        {
            tarifas.Add(1); tarifas.Add(2); tarifas.Add(3); tarifas.Add(4);
            tarifas_monedas.Add(1); tarifas_monedas.Add(2); tarifas_monedas.Add(4); tarifas_monedas.Add(5); tarifas_monedas.Add(6);
        }

        public static string valorTarifa(int valor) { 
            switch (valor){
                case D:
                    return "Desde";
                case P:
                    return "PDF";
                case L:
                    return "Llamar";
                case V:
                    return "VerDetalle";
                default:
                    return "";
            }
        }

        public static string valorTarifaChar(int valor)
        {
            switch (valor)
            {
                case D:
                    return "D";
                case P:
                    return "P";
                case L:
                    return "L";
                case V:
                    return "V";
                default:
                    return "";
            }
        }

        public static string valorTarifaMoneda(int valor)
        {
            switch (valor)
            {
                case DPN:
                    return "DPN";
                case EUROS:
                    return "€";
                case DOLAR:
                    return "$";
                case BSF:
                    return "Bs";
                case BSS:
                    return "BsS";
                default:
                    return "";
            }
        } 

    }
}
