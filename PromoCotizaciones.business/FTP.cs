﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using passarini.web.business.transacciones;


namespace PromoCotizaciones.business
{
    public class FTP
    {

        private string ftpServerIP;
        private string ftpUserName;
        private string ftpPassword;


        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public FTP()
        {
            ftpServerIP = System.Configuration.ConfigurationManager.AppSettings["ftpIp"];
            ftpUserName = System.Configuration.ConfigurationManager.AppSettings["ftpUsername"];
            ftpPassword = System.Configuration.ConfigurationManager.AppSettings["ftpPassword"];
        }

        public List<string> DirectoryListing(string Path)
        {
            List<string> result = new List<string>();

            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://" + this.ftpServerIP + Path);
            request.Credentials = new NetworkCredential(this.ftpUserName, this.ftpPassword);

            request.Method = WebRequestMethods.Ftp.ListDirectory;

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            Stream responseStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(responseStream);

            

            while (!reader.EndOfStream)
            {
                string valor = reader.ReadLine();
                try
                {
                    valor = valor.Substring(valor.LastIndexOf("/")); ;
                }
                catch
                {
                }
                result.Add(valor);
            }

            reader.Close();
            response.Close();

            return result;
        }


        public void DeleteFTPFile(string Path)
        {
            FtpWebRequest clsRequest = (System.Net.FtpWebRequest)WebRequest.Create("ftp://" + this.ftpServerIP + Path);
            clsRequest.Credentials = new System.Net.NetworkCredential(this.ftpUserName, this.ftpPassword);

            clsRequest.Method = WebRequestMethods.Ftp.DeleteFile;

            string result = string.Empty;
            FtpWebResponse response = (FtpWebResponse)clsRequest.GetResponse();
            long size = response.ContentLength;
            Stream datastream = response.GetResponseStream();
            StreamReader sr = new StreamReader(datastream);
            result = sr.ReadToEnd();
            sr.Close();
            datastream.Close();
            response.Close();
        }

        public void DeleteFTPDirectory(string Path)
        {
            List<string> filesList = DirectoryListing(Path);

            foreach (string file in filesList)
            {
                DeleteFTPFile(Path + "/" + file);
            }

        }


        public void CreateFTPDirectory(string Path)
        {
            WebRequest ftpRequest = WebRequest.Create("ftp://" + this.ftpServerIP + Path);
            ftpRequest.Method = WebRequestMethods.Ftp.MakeDirectory;
            ftpRequest.Credentials = new NetworkCredential(this.ftpUserName, this.ftpPassword);
            WebResponse response = ftpRequest.GetResponse();
        }

        public void UploadFTPFile(string Path, FileInfo fot)
        {

            FtpWebRequest ftpClient = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + this.ftpServerIP + Path));
            ftpClient.Credentials = new System.Net.NetworkCredential(this.ftpUserName, this.ftpPassword);
            ftpClient.Method = System.Net.WebRequestMethods.Ftp.UploadFile;
            ftpClient.UseBinary = true;
            ftpClient.KeepAlive = true;
            System.IO.FileInfo fi = new System.IO.FileInfo(fot.FullName);
            ftpClient.ContentLength = fi.Length;
            byte[] buffer = new byte[4097];
            int bytes = 0;
            int total_bytes = (int)fi.Length;
            System.IO.FileStream fs = fi.OpenRead();
            System.IO.Stream rs = ftpClient.GetRequestStream();
            while (total_bytes > 0)
            {
                bytes = fs.Read(buffer, 0, buffer.Length);
                rs.Write(buffer, 0, bytes);
                total_bytes = total_bytes - bytes;
            }
            //fs.Flush();
            fs.Close();
            rs.Close();
            FtpWebResponse uploadResponse = (FtpWebResponse)ftpClient.GetResponse();
            uploadResponse.Close();
        
        }
        

    }
}
