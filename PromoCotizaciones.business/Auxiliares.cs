using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net.Mime;
using passarini.web.business.transacciones;
using passarini.web.business.seguridad;


namespace PromoCotizaciones.business
{
	/// <summary>
	/// Maneja la lista de tablas auxiliares
	/// </summary>
    public class Auxiliares : Base
	{

        /// <summary>
        /// Lista los asesores de acuerdo a los filtros dados
        /// </summary>
        public DataTable listarAsesores()
        {
            try
            {
                base.beginBaseTransaction();

                DataTable temp = this.listarAsesores(-1, "");

                base.commitBaseTransaction();

                return temp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Lista los asesores de acuerdo a los filtros dados
        /// </summary>
        public DataTable listarAsesores(int codigoAsesor)
        {
            try
            {
                base.beginBaseTransaction();

                DataTable temp = this.listarAsesores(codigoAsesor, "");

                base.commitBaseTransaction();

                return temp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Lista los asesores de acuerdo a los filtros dados
        /// </summary>
        public DataTable listarAsesores(int codigoAsesor, String nombre)
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds;

                ad = this.getBaseDataAdapter("select * from aux_asesor where (codigo_asesor = @codigoAsesor or @codigoAsesor is null) and (nombre like '%' + @nombre + '%' or @nombre = '') order by codigo_asesor;");
                ((SqlParameter)ad.SelectCommand.Parameters.Add("@codigoAsesor", SqlDbType.Int)).Value = (codigoAsesor < 0 ? DBNull.Value : (Object)codigoAsesor);
                ((SqlParameter)ad.SelectCommand.Parameters.Add("@nombre", SqlDbType.VarChar)).Value = nombre;
                ds = new DataSet();
                ad.Fill(ds, "Tabla");

                base.commitBaseTransaction();

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Agrega un nuevo asesor y devuelve su Id.
        /// </summary>
        public int agregarAsesor(String nombre, bool es_supervisor)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com = this.getBaseCommand();
                int temp;

                com.CommandText = "insert into aux_asesor (nombre, supervisor) values (@nombre, @es_supervisor);";
                com.Parameters.Clear();
                ((SqlParameter)com.Parameters.Add("@nombre", SqlDbType.VarChar)).Value = nombre;
                ((SqlParameter)com.Parameters.Add("@es_supervisor", SqlDbType.Bit)).Value = es_supervisor;
                com.ExecuteNonQuery();

                com.CommandText = "select @@identity;";
                com.Parameters.Clear();
                temp = Decimal.ToInt32((Decimal)com.ExecuteScalar());

                base.commitBaseTransaction();

                return temp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Elimina el asesor dado, s�lo si no ha sido referenciado
        /// </summary>
        public void eliminarAsesor(int codigoAsesor)
        {
            try
            {
                base.beginBaseTransaction();

                if (this.haSidoReferenciadoAsesor(codigoAsesor))
                {
                    throw new Exception("No se puede eliminar un agente que haya sido referenciado en otras tablas. Elimine primero dichas referencias.");
                }

                SqlCommand com;

                com = this.getBaseCommand("delete from aux_asesor where codigo_asesor = @codigo_asesor");
                ((SqlParameter)com.Parameters.Add("@codigo_asesor", SqlDbType.Int)).Value = codigoAsesor;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Indica si un asesor ha sido referenciado
        /// </summary>
        private bool haSidoReferenciadoAsesor(int codigoAsesor)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;
                int temp;

                com = this.getBaseCommand("select cast(count(*) as decimal) from ((select top 1 codigo_asesor from Cot_Cotizacion where codigo_asesor = @codigo_asesor) union (select top 1 asociado_a as codigo_asesor from sec_usuario_rol where codigo_rol = @codigo_rol_cotizar and asociado_a = @codigo_asesor)) t;");
                ((SqlParameter)com.Parameters.Add("@codigo_asesor", SqlDbType.Int)).Value = codigoAsesor;
                ((SqlParameter)com.Parameters.Add("@codigo_rol_cotizar", SqlDbType.Int)).Value = Rol.rolCotizar;
                temp = Decimal.ToInt32((Decimal)com.ExecuteScalar());

                base.commitBaseTransaction();

                return (temp > 0);
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }

        }


        /// <summary>
        /// Env�a un email del website
        /// </summary>
        public void enviarEmail(String mailTo, String mailCC, String mailSubject, String mailBody)
        {
            try
            {
                base.beginBaseTransaction();

                var server = this.obtenerValorParametro("mail_server");
                var port = Int32.Parse(this.obtenerValorParametro("mail_port"));
                var smtpUser = this.obtenerValorParametro("mail_username");
                var pwd = this.obtenerValorParametro("mail_password");
                var ssl = (this.obtenerValorParametro("mail_ssl") == "1");
                var from = this.obtenerValorParametro("mail_from");

                var client = new SmtpClient(server, port)
                {
                    Credentials = new System.Net.NetworkCredential(smtpUser, pwd),
                    EnableSsl = ssl
                };

                var msg = new MailMessage()
                {
                    IsBodyHtml = false,
                    Body = mailBody.Replace("\r\n", "\n").Replace("\n", "\r\n"),
                    From = new MailAddress(from),
                    Subject = mailSubject
                };

                var emails = mailTo.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var email in emails)
                    msg.To.Add(new MailAddress(email.Trim()));

                emails = mailCC.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var email in emails)
                    msg.CC.Add(new MailAddress(email.Trim()));


                client.Send(msg);

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Env�a un email del website, cuyo cuerpo en HTML sea el URL dado
        /// <param name="inlineAttachments">
        ///     Tabla que contiene los inline attachments (ej. imagenes) dentro del correo.
        ///     Posee las columnas:
        /// 
        ///       FileName: ubicacion en disco del archivo
        ///       ContentType: tipo de contenido MIME
        ///       URL: URL a sustituir en el tag IMG del HTML
        /// </param>
        /// </summary>
        public void enviarEmailHTML(String mailFrom, String mailTo, String mailCC, String mailSubject, String mailURL, System.Collections.Generic.List<InlineAttachment> mailAttachments)
        {
            try
            {
                base.beginBaseTransaction();

                System.Net.WebClient web = new System.Net.WebClient();
                web.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1468.0 Safari/537.36");

                var server = this.obtenerValorParametro("mail_server");
                var port = Int32.Parse(this.obtenerValorParametro("mail_port"));
                var smtpUser = this.obtenerValorParametro("mail_username");
                var pwd = this.obtenerValorParametro("mail_password");
                var ssl = (this.obtenerValorParametro("mail_ssl") == "1");

                var client = new SmtpClient(server, port)
                {
                    Credentials = new System.Net.NetworkCredential(smtpUser, pwd),
                    EnableSsl = ssl
                };

                var msg = new MailMessage()
                {
                    IsBodyHtml = true,
                    Body = web.DownloadString(mailURL.Replace("https://", "http://")),
                    From = new MailAddress(mailFrom),
                    Subject = mailSubject
                };

                var emails = mailTo.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var email in emails)
                    msg.To.Add(new MailAddress(email.Trim()));

                emails = mailCC.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var email in emails)
                    msg.CC.Add(new MailAddress(email.Trim()));

                foreach (InlineAttachment inlineAttachment in mailAttachments)
                {
                    Attachment inline = new Attachment(inlineAttachment.fileName);
                    inline.ContentDisposition.Inline = true;
                    inline.ContentDisposition.DispositionType = DispositionTypeNames.Inline;
                    inline.ContentType.MediaType = inlineAttachment.contentType;
                    if (msg.Body.Contains(inlineAttachment.url))
                    {
                        msg.Body = msg.Body.Replace(inlineAttachment.url, "cid:" + inline.ContentId);
                        msg.Attachments.Add(inline);
                    }
                }

                client.Send(msg);

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Env�a un email del website, cuyo cuerpo en HTML sea el URL dado
        /// </summary>
        public void enviarEmailHTML(String to, String cc, String subject, String url)
        {
            try
            {
                base.beginBaseTransaction();

                this.enviarEmailHTML(this.obtenerValorParametro("mail_from"), to, cc, subject, url, new System.Collections.Generic.List<InlineAttachment>());

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Env�a un email del website, cuyo cuerpo en HTML sea el URL dado con los inline attachments dados
        /// </summary>
        public void enviarEmailHTML(String to, String cc, String subject, String url, System.Collections.Generic.List<InlineAttachment> inlineAttachments)
        {
            try
            {
                base.beginBaseTransaction();

                this.enviarEmailHTML(this.obtenerValorParametro("mail_from"), to, cc, subject, url, inlineAttachments);

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Env�a un email del website, cuyo cuerpo en HTML sea el URL dado con los inline attachments dados
        /// </summary>
        public void enviarEmailHTML(String from, String to, String cc, String subject, String url)
        {
            try
            {
                base.beginBaseTransaction();

                this.enviarEmailHTML(from, to, cc, subject, url, new System.Collections.Generic.List<InlineAttachment>());

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Constructor de la clase. Recibe un contexto de conexi�n a base de datos.
        /// </summary>
        public Auxiliares(BaseContext baseContext) : base(baseContext)
		{
            try
            {
                base.beginBaseTransaction();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Devuelve el valor de un par�metro dado
        /// </summary>
        public String obtenerValorParametro(String codigoParametro)
        {
            string valor = System.Configuration.ConfigurationManager.AppSettings[codigoParametro];
            return valor;
        }

		
	}

    /// <summary>
    /// Clase que representa un inline attachment, utilizada por Auxiliares.enviarEmailHTML()
    /// </summary>
    public class InlineAttachment
    {
        public String fileName;
        public String url;
        public String contentType;

        ///       FileName: ubicacion en disco del archivo
        ///       ContentType: tipo de contenido MIME
        ///       URL: URL a sustituir en el tag IMG del HTML

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        /// <param name="fileName">Ubicacion en el disco del attachment</param>
        /// <param name="url">URL a sustituir en el tag IMG del HTML</param>
        /// <param name="contentType">Tipo de contenido MIME</param>
        public InlineAttachment(String fileName, String url, String contentType)
        {
            this.fileName = fileName;
            this.url = url;
            this.contentType = contentType;
        }
    }
}
