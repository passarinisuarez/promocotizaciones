﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PromoCotizaciones.Utilities
{
    public class pdfUtilities
    {

        public static void ConvertHtmlToPdf(string path, string text)
        {
            StringBuilder sbHtmlText = new StringBuilder();
            sbHtmlText.Append(text);

            Document document = new Document();
            PdfWriter.GetInstance(document, new FileStream(path, FileMode.Create));
            document.Open();
            HTMLWorker hw = new HTMLWorker(document);
            hw.Parse(new StringReader(sbHtmlText.ToString()));
            document.Close();
        }    
    }

    

    
}
