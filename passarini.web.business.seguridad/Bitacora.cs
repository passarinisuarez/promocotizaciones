using System;
using System.Data;
using System.Data.SqlClient;
using passarini.web.business.transacciones;

namespace passarini.web.business.seguridad
{

    /// <summary>
    /// Esta clase maneja todo lo relacionado con la bit�cora de actividad.
    /// </summary>
    public class Bitacora : Base
    {

        /// <summary>
        /// Lista los elementos en la bit�cora de acuerdo a los filtros dados
        /// </summary>
        public DataTable listarBitacora(DateTime desdeFechaHora, DateTime hastaFechaHora, string login, int codigoTransaccion, string observaciones)
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds = new DataSet();

                ad = base.getBaseDataAdapter("select b.*, t.nombre as nombre_transaccion, isnull(u.nombre,'') as nombre_usuario from sec_bitacora b inner join sec_transaccion t on b.codigo_transaccion = t.codigo_transaccion left join sec_usuario u on b.login = u.login where b.fecha_hora >= @desde and b.fecha_hora <= @hasta and (b.login = @login or @login = '') and (b.codigo_transaccion = @codigo_transaccion or @codigo_transaccion < 0) and b.observaciones like @observaciones order by b.fecha_hora desc;");
                ad.SelectCommand.Parameters.Add("@desde", SqlDbType.DateTime).Value = desdeFechaHora;
                ad.SelectCommand.Parameters.Add("@hasta", SqlDbType.DateTime).Value = hastaFechaHora;
                ad.SelectCommand.Parameters.Add("@login", SqlDbType.VarChar).Value = login;
                ad.SelectCommand.Parameters.Add("@codigo_transaccion", SqlDbType.Int).Value = codigoTransaccion;
                ad.SelectCommand.Parameters.Add("@observaciones", SqlDbType.VarChar).Value = "%" + observaciones + "%";
                ad.Fill(ds, "Tabla");

                base.commitBaseTransaction();

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Limpia la bit�cora. Registra una l�nea indicando el usuario que limpi� la bit�cora.
        /// </summary>
        /// <param name="login"></param>
        public void limpiarBitacora(string login)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("delete from sec_bitacora;");
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Registra la transacci�n dada en la bit�cora
        /// </summary>
        public void registrarEnBitacora(string login, int codigoTransaccion, string observaciones)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("insert into sec_bitacora (Fecha_Hora, Login, Codigo_Transaccion, Observaciones) values (@Fecha_Hora, @Login, @Codigo_Transaccion, @Observaciones);");
                com.Parameters.Add("@fecha_hora", SqlDbType.DateTime).Value = System.DateTime.Now;
                com.Parameters.Add("@login", SqlDbType.VarChar).Value = login;
                com.Parameters.Add("@codigo_transaccion", SqlDbType.Int).Value = codigoTransaccion;
                com.Parameters.Add("@observaciones", SqlDbType.VarChar).Value = observaciones;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Constructor de la clase. Recibe un contexto de conexi�n a base de datos.
        /// </summary>
        public Bitacora(BaseContext baseContext)
            : base(baseContext)
        {
        }

    }

}