using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using passarini.web.business.transacciones;

namespace passarini.web.business.seguridad
{
    /// <summary>
    /// Esta clase representa a un Rol definido.
    /// </summary>
    public class Rol : Base
    {
        public readonly int codigoRol;
        public string nombre;
        public string descripcion;
        public string queryAsociadoA;

        public const int rolAdministradorDelSistema = 1;
        public const int rolPublicarPromo = 2;
        public const int rolCrear_EditarPromo = 3;
        public const int rolVerPromo = 4;

        public const int rolCotizar = 5;
        public const int rolSupervisorCotizaciones = 6;
        public const int rolVerCotizar = 7;

        /// <summary>
        /// Lista las transacciones permitidas para el rol
        /// </summary>
        public DataTable listarTransaccionesPermitidas()
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds = new DataSet();

                ad = base.getBaseDataAdapter("select t.* from SEC_Rol_Transaccion rt inner join SEC_Transaccion t on rt.codigo_transaccion = t.codigo_transaccion where rt.codigo_rol = @codigo_rol order by t.nombre;");
                ad.SelectCommand.Parameters.Add("@codigo_rol", SqlDbType.Int).Value = this.codigoRol;
                ad.Fill(ds, "Tabla");

                base.commitBaseTransaction();

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }

        }

        /// <summary>
        /// Devuelve el resultado del query correspondiente al campo QueryAsociadoA
        /// </summary>
        public DataTable listarQueryAsociadoA()
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds = new DataSet();

                ad = base.getBaseDataAdapter(this.queryAsociadoA);
                ad.Fill(ds, "Tabla");

                base.commitBaseTransaction();

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }

        }

        /// <summary>
        /// Registra una transacción como permitida
        /// </summary>
        public void registrarTransaccionPermitida(int codigoTransaccion)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;
                
                com = base.getBaseCommand("insert into SEC_Rol_Transaccion (codigo_rol, codigo_transaccion) values (@codigo_rol, @codigo_transaccion);");
                com.Parameters.Add("@codigo_rol", SqlDbType.Int).Value = codigoRol;
                com.Parameters.Add("@codigo_transaccion", SqlDbType.Int).Value = codigoTransaccion;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Elimina una transacción de la lista de permitidas
        /// </summary>
        public void eliminarTransaccionPermitida(int codigoTransaccion)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("delete from sec_rol_transaccion where codigo_rol = @codigo_rol and codigo_transaccion = @codigo_transaccion;");
                com.Parameters.Add("@codigo_rol", SqlDbType.Int).Value = codigoRol;
                com.Parameters.Add("@codigo_transaccion", SqlDbType.Int).Value = codigoTransaccion;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Indica si el rol tiene permiso para determinada transacción
        /// </summary>
        public bool tienePermiso(int codigoTransaccion)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;
                SqlDataReader sqldr;
                bool temp;

                com = base.getBaseCommand("select * from SEC_Rol_Transaccion where codigo_rol = @codigo_rol and codigo_transaccion = @codigo_transaccion;");
                com.Parameters.Add("@codigo_rol", SqlDbType.Int).Value = codigoRol;
                com.Parameters.Add("@codigo_transaccion", SqlDbType.Int).Value = codigoTransaccion;
                sqldr = com.ExecuteReader();
                temp = sqldr.Read();
                sqldr.Close();

                base.commitBaseTransaction();

                return temp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Actualiza en la base de datos las propiedades de acuerdo a sus valores en memoria
        /// </summary>
        public void guardar()
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("update SEC_Rol set nombre = @nombre, descripcion = @descripcion, query_asociado_a = @query_asociado_a where codigo_rol = @codigo_rol;");
                com.Parameters.Add("@codigo_rol", SqlDbType.Int).Value = this.codigoRol;
                com.Parameters.Add("@nombre", SqlDbType.VarChar).Value = this.nombre;
                com.Parameters.Add("@descripcion", SqlDbType.VarChar).Value = this.descripcion;
                com.Parameters.Add("@query_asociado_a", SqlDbType.VarChar).Value = this.queryAsociadoA;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public Rol(BaseContext baseContext, int codigoRol) : base(baseContext)
        {
            try
            {
                base.beginBaseTransaction();

                this.codigoRol = codigoRol;
                
                SqlCommand com;
                SqlDataReader sqldr;
                
                com = base.getBaseCommand("select * from SEC_Rol where codigo_rol = @codigo_rol;");
                com.Parameters.Add("@codigo_rol", SqlDbType.Int).Value = codigoRol;
                sqldr = com.ExecuteReader();
                sqldr.Read();
                this.nombre = (String)sqldr["nombre"];
                this.descripcion = (String)sqldr["descripcion"];
                this.queryAsociadoA = (String)sqldr["query_asociado_a"];
                sqldr.Close();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }
    }

}
