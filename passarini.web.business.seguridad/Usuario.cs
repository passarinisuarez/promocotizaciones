using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using passarini.web.business.transacciones;

namespace passarini.web.business.seguridad
{
    /// <summary>
    /// Esta clase representa a un usuario definido.
    /// </summary>
    public class Usuario : Base
    {
        public readonly string login;
        public string nombre;
        public string descripcion;
        public string email;
        public string observaciones;
        public readonly bool activo;
        public readonly string preguntaOlvidoPassword;
        public object codigo_recordatorio;
        
        protected string salt;

        /// <summary>
        /// Actualiza en la base de datos las propiedades de acuerdo a sus valores en memoria
        /// </summary>
        public void guardar()
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;
                
                com = base.getBaseCommand("update SEC_Usuario set nombre = @nombre, descripcion = @descripcion, email = @email, observaciones = @observaciones where login = @login;");
                com.Parameters.Add("@login", SqlDbType.VarChar).Value = this.login;
                com.Parameters.Add("@nombre", SqlDbType.VarChar).Value = this.nombre;
                com.Parameters.Add("@descripcion", SqlDbType.VarChar).Value = this.descripcion;
                com.Parameters.Add("@email", SqlDbType.VarChar).Value = this.email;
                com.Parameters.Add("@observaciones", SqlDbType.VarChar).Value = this.observaciones;
                com.ExecuteNonQuery();
                
                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Obtiene las claves a enviar a la cookie, los datos son (en orden):
        /// * Clave de encriptacion (GUID encriptado)
        /// * Password encriptado
        /// * Login encriptado
        /// Si se ejecuta este metodo y ya existe un codigo recordatorio generado, se envia un error, se recomienda que en el Caller se revoque la sesion inmediatamente y si es posible se bloquee el acceso al ip preventivamente
        /// </summary>
        /// <param name="password">Contrase�a del usuario</param>
        /// <returns>La lista de valores a almacenar en una cookie segura</returns>
        public List<string> obtenerRecodatorioParaCookie(string password)
        {
            try
            {
                base.beginBaseTransaction();

                List<string> list = new List<string>();
                // Se genera un Guid y se encripta, esto forma parte de las claves de la cookie
                Guid guid = Guid.NewGuid();
                string encGuid = Util.encriptar(Util.encriptar(guid.ToString(), Util.CryptoAlgorithm.MD5) + salt);

                string encPassword = Util.encriptar(Util.encriptar(password) + salt);

                SqlCommand com;
                com = base.getBaseCommand("UPDATE sec_usuario SET codigo_recordatorio = @codigo_recordatorio, codigo_recordatorio_encriptado = @codigo_recordatorio_encriptado WHERE login = @login AND password = @password AND codigo_recordatorio is null");
                com.Parameters.Add("@login", SqlDbType.VarChar).Value = this.login;
                com.Parameters.Add("@password", SqlDbType.VarChar).Value = encPassword;
                com.Parameters.Add("@codigo_recordatorio", SqlDbType.VarChar).Value = guid.ToString();
                com.Parameters.Add("@codigo_recordatorio_encriptado", SqlDbType.VarChar).Value = encGuid;
                com.Parameters.Add("@observaciones", SqlDbType.VarChar).Value = this.observaciones;
                int counter = com.ExecuteNonQuery();

                if (counter == 0) // Usuario no existe o el codigo recordatorio esta seteado
                {
                    throw new System.Security.SecurityException("El usuario ya se recuerda o no existe, posible ataque en proceso");
                }

                list.Add(encGuid);
                list.Add(Util.encriptar(encPassword + guid));
                list.Add(Util.encriptar(Util.encriptar(Util.encriptar(this.login) + salt, Util.CryptoAlgorithm.MD5)+ guid));
                
                base.commitBaseTransaction();
                return list;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Autentica el usuario con las claves de las cookies
        /// </summary>
        /// <param name="clave">Clave unica generada para identificar el almacenamiento</param>
        /// <param name="cookieLogin">Login del usuario en cookie</param>
        /// <param name="cookiePassword">Contrasena del usuario en cookie</param>
        /// <returns>True si se logra autenticar.  Arroja una excepcion de tipo SecurityException si hay alguna inconsistencia.
        /// De ocurrir una excepcion se recomienda revocar la sesion del usuario, pudiendo bloquearse preventivamente</returns>
        bool autenticarPorCookie(string clave, string cookieLogin, string cookiePassword)
        {
            try
            {
                base.beginBaseTransaction();
                string sql = "select * from sec_usuario where codigo_recordatorio_encriptado = @codigo_recordatorio_encriptado";

                DataSet ds = new DataSet();
                System.Data.SqlClient.SqlDataAdapter adapter = base.getBaseDataAdapter(sql);

                adapter.Fill(ds);

                // Valida que el dataset contenga datos... 
                if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                {
                    throw new System.Security.SecurityException("Los datos del usuario no existen, posible ataque en progreso");
                }

                if (ds.Tables[0].Rows.Count != 1)
                {
                    throw new System.Security.SecurityException("Dos o mas usuarios con el mismo codigo recordatorio, es posible que los datos hayan sido corrompidos");
                }
                
                DataRow dr = ds.Tables[0].Rows[0];
                string codigo_recordatorio = dr["codigo_recordatorio"].ToString();
                string codigo_recordatorio_encriptado = dr["codigo_recordatorio_encriptado"].ToString();
                string _login = dr["login"].ToString();
                string _password = dr["password"].ToString();

                // Paso 1: Validar consistencia del codigo recordatorio con el encriptado
                if (codigo_recordatorio_encriptado != Util.encriptar(Util.encriptar(codigo_recordatorio, Util.CryptoAlgorithm.MD5) + this.salt))
                {
                    // Codigo recibido no corresponde al del usuario, posible ataque
                    throw new System.Security.SecurityException("El codigo recordatorio no coincide, posible ataque en proceso");
                }

                // Paso 2: Validar que el usuario y el password correspondan a los enviados
                string encPassword = Util.encriptar(_password + codigo_recordatorio);
                string encLogin = Util.encriptar(Util.encriptar(Util.encriptar(_login) + salt, Util.CryptoAlgorithm.MD5) + codigo_recordatorio);

                if (encLogin != cookieLogin || encPassword != cookiePassword)
                {
                    // El codigo enviado no corresponde al usuario o contrasena, no se logea el usuario
                    throw new System.Security.SecurityException("El usuario o password no coincide, posible ataque en proceso");
                }
                base.commitBaseTransaction();
                return true;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }


        /// <summary>
        /// Lista los roles a los que pertenece el usuario
        /// </summary>
        public DataTable listarRoles()
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds = new DataSet();
                
                ad = base.getBaseDataAdapter("select r.* from SEC_Usuario_Rol ur inner join SEC_Rol r on ur.codigo_rol = r.codigo_rol where ur.login = @login order by r.nombre;");
                ad.SelectCommand.Parameters.Add("@login", SqlDbType.VarChar).Value = login;
                ad.Fill(ds, "Tabla");

                base.commitBaseTransaction();

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Devuelve True si el usuario pertenece al rol dado
        /// </summary>
        public bool perteneceARol(int codigoRol)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;
                bool pertenece;
                
                com = base.getBaseCommand("select count(*) from SEC_Usuario_Rol where login = @login and codigo_rol = @codigo_rol;");
                com.Parameters.Add("@login", SqlDbType.VarChar).Value = this.login;
                com.Parameters.Add("@codigo_rol", SqlDbType.Int).Value = codigoRol;
                pertenece = (int.Parse(com.ExecuteScalar().ToString()) > 0);

                base.commitBaseTransaction();

                return pertenece;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Obtiene el c�digo de la entidad a la que est� asociado el usuario para determinado rol
        /// Ejemplo: para el rol Cliente devolver� el c�digo del cliente asociado al usuario
        /// Devuelve -1 si no est� asociado a nadie
        /// </summary>
        public int obtenerAsociacionDeRol(int codigoRol)
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds = new DataSet();
                
                ad = base.getBaseDataAdapter("select ur.asociado_a from SEC_Usuario_Rol ur inner join SEC_Rol r on ur.codigo_rol = r.codigo_rol where ur.login = @login and r.codigo_rol = @codigo_rol;");
                ad.SelectCommand.Parameters.Add("@login", SqlDbType.VarChar).Value = login;
                ad.SelectCommand.Parameters.Add("@codigo_rol", SqlDbType.Int).Value = codigoRol;
                ad.Fill(ds, "Tabla");

                base.commitBaseTransaction();

                if (ds.Tables[0].Rows.Count == 0)
                {
                    return -1;
                }
                else
                {
                    return int.Parse(ds.Tables[0].Rows[0]["asociado_a"].ToString());
                }
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Registra al usuario en el rol dado
        /// AsociadoA es el cliente o agente en caso de que pertenezca a dicho rol. 
        /// </summary>
        /// <param name="codigoRol"></param>
        /// <param name="asociadoA"></param>
        public void registrarEnRol(int codigoRol, int asociadoA)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;
                
                com = base.getBaseCommand("delete from SEC_Usuario_Rol where login = @login and codigo_rol = @codigo_rol;");
                com.Parameters.Add("@login", SqlDbType.VarChar).Value = this.login;
                com.Parameters.Add("@codigo_rol", SqlDbType.Int).Value = codigoRol;
                com.Parameters.Add("@asociado_a", SqlDbType.Int).Value = (asociadoA < 0 ? DBNull.Value : (object)asociadoA);
                com.ExecuteNonQuery();
                com.CommandText = "insert into SEC_Usuario_Rol (login, codigo_rol, asociado_a) values (@login, @codigo_rol, @asociado_a);";
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Elimina al usuario del rol dado
        /// </summary>
        public void eliminarDeRol(int codigoRol)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;
                
                com = base.getBaseCommand("delete from SEC_Usuario_Rol where login = @login and codigo_rol = @codigo_rol;");
                com.Parameters.Add("@login", SqlDbType.VarChar).Value = this.login;
                com.Parameters.Add("@codigo_rol", SqlDbType.Int).Value = codigoRol;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Autentica al usuario, dado su password
        /// </summary>
        public bool autenticar(string password)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;
                SqlDataReader sqldr;
                bool temp;
                
                com = base.getBaseCommand("select * from SEC_Usuario where login = @login and password = @password and activo = 1;");
                com.Parameters.Add("@login", SqlDbType.VarChar).Value = this.login;
                com.Parameters.Add("@password", SqlDbType.VarChar).Value = Util.encriptar(Util.encriptar(password) + this.salt);
                sqldr = com.ExecuteReader();
                temp = sqldr.Read();
                sqldr.Close();

                base.commitBaseTransaction();
                
                return temp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Autentica al usuario, dada su respuesta de olvido de password
        /// </summary>
        public bool autenticarPorRespuestaOlvidoPassword(string respuestaOlvidoPassword)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;
                SqlDataReader sqldr;
                bool temp;
                
                com = base.getBaseCommand("select * from SEC_Usuario where login = @login and respuesta_olvido_password = @respuesta_olvido_password and activo = 1;");
                com.Parameters.Add("@login", SqlDbType.VarChar).Value = this.login;
                com.Parameters.Add("@respuesta_olvido_password", SqlDbType.VarChar).Value = Util.encriptar(respuestaOlvidoPassword);
                sqldr = com.ExecuteReader();
                temp = sqldr.Read();
                sqldr.Close();

                base.commitBaseTransaction();

                return temp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Activa al usuario
        /// </summary>
        public void activar()
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;
                
                com = base.getBaseCommand("update SEC_Usuario set activo = 1 where login = @login;");
                com.Parameters.Add("@login", SqlDbType.VarChar).Value = this.login;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Desactiva al usuario
        /// </summary>
        public void desactivar()
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;
                
                com = base.getBaseCommand("update SEC_Usuario set activo = 0 where login = @login;");
                com.Parameters.Add("@login", SqlDbType.VarChar).Value = this.login;
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Cambia el password, pregunta y respuesta de olvido de password para el usuario
        /// </summary>
        public void cambiarPassword(string password, string preguntaOlvidoPassword, string respuestaOlvidoPassword)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;
                
                com = base.getBaseCommand("update SEC_Usuario set password = @password, pregunta_olvido_password = @pregunta_olvido_password, respuesta_olvido_password = @respuesta_olvido_password where login = @login;");
                com.Parameters.Add("@login", SqlDbType.VarChar).Value = this.login;
                com.Parameters.Add("@password", SqlDbType.VarChar).Value = Util.encriptar(Util.encriptar(password) + this.salt);
                com.Parameters.Add("@pregunta_olvido_password", SqlDbType.VarChar).Value = preguntaOlvidoPassword;
                com.Parameters.Add("@respuesta_olvido_password", SqlDbType.VarChar).Value = Util.encriptar(respuestaOlvidoPassword);
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Cambia el password para el usuario
        /// </summary>
        public void cambiarPassword(string password)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("update SEC_Usuario set password = @password where login = @login;");
                com.Parameters.Add("@login", SqlDbType.VarChar).Value = this.login;
                com.Parameters.Add("@password", SqlDbType.VarChar).Value = Util.encriptar(Util.encriptar(password) + this.salt);
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Indica si el usuario tiene permiso para efectuar cierta transacci�n
        /// </summary>
        public bool tienePermiso(int codigoTransaccion)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;
                SqlDataReader sqldr;
                bool temp;

                com = base.getBaseCommand("select * from SEC_Usuario_Rol ur inner join SEC_Rol_Transaccion rt on ur.codigo_rol = rt.codigo_rol where ur.login = @login and rt.codigo_transaccion = @codigo_transaccion;");
                com.Parameters.Add("@login", SqlDbType.VarChar).Value = this.login;
                com.Parameters.Add("@codigo_transaccion", SqlDbType.Int).Value = codigoTransaccion;
                sqldr = com.ExecuteReader();
                temp = sqldr.Read();
                sqldr.Close();

                base.commitBaseTransaction();

                return temp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Deslogea el usuario de la cuenta, elimina los datos de la sesion almacenados en BD
        /// </summary>
        /// <returns></returns>
        public void Deslogear()
        {
            try
            {
                this.beginBaseTransaction(); 

                SqlCommand command = base.getBaseCommand("UPDATE SEC_Usuario SET codigo_recordatorio = null, codigo_recordatorio_encriptado = null WHERE login = @login");
                command.Parameters.Add("@login", SqlDbType.VarChar).Value = this.login;

                command.ExecuteNonQuery();
                this.commitBaseTransaction();
            }
            catch (Exception e)
            {
                base.rollbackBaseTransaction();
                throw e;
            }
        }

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public Usuario(BaseContext baseContext, string login) : base(baseContext)
        {
            try
            {
                base.beginBaseTransaction();

                this.login = login;
                
                SqlCommand com;
                SqlDataReader sqldr;
                
                com = base.getBaseCommand("select * from SEC_Usuario where login = @login;");
                com.Parameters.Add("@login", SqlDbType.VarChar).Value = login;
                sqldr = com.ExecuteReader();
                sqldr.Read();
                this.nombre = (string)sqldr["nombre"];
                this.descripcion = (string)sqldr["descripcion"];
                this.email = (string)sqldr["email"];
                this.observaciones = (string)sqldr["observaciones"];
                this.activo = (bool)sqldr["activo"];
                this.preguntaOlvidoPassword = (string)sqldr["pregunta_olvido_password"];
                this.salt = (string)sqldr["salt"];
                this.codigo_recordatorio = sqldr["codigo_recordatorio"];
                sqldr.Close();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }
    }

}
