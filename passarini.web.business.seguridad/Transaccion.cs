using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using passarini.web.business.transacciones;

namespace passarini.web.business.seguridad
{

    /// <summary>
    /// Esta clase representa a una Transacción definida.
    /// </summary>
    public class Transaccion : Base
    {
        public readonly int codigoTransaccion;
        public string nombre;
        public string descripcion;
        public string grupo;
        
        // Transacciones: Administración
        public const int transAdministrarSeguridad = 1;
        public const int transAdministrarParametros = 9;
        public const int transAdministrarClientes = 10;
        public const int transAdministrarAgentes = 11;

        // Transacciones: Solicitud de Boletos
        public const int transSolicitudesBoletosConsultar = 2;
        public const int transSolicitudesBoletosRegistrar = 3;
        public const int transSolicitudesBoletosConsultarTodosAgentesYClientes = 5;
        public const int transSolicitudesBoletosReservar = 6;
        public const int transSolicitudesBoletosAnular = 7;
        public const int transSolicitudesBoletosColocarNroFactura = 8;
        public const int transConsultarSolicitudesDeTodosLosUsuarios = 13;

        // Transacciones: Website
        public const int transVerFormulariosDeContacto = 14;

        // Transacciones: Sabre
        public const int transSabreReservaDeVuelos = 12;

        /// <summary>
        /// Actualiza en la base de datos las propiedades de acuerdo a sus valores en memoria
        /// </summary>
        public void guardar()
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand("update SEC_Transaccion set nombre = @nombre, descripcion = @descripcion, grupo = @grupo where codigo_transaccion = @codigo_transaccion;");
                com.Parameters.Add("@codigo_transaccion", SqlDbType.Int).Value = this.codigoTransaccion;
                com.Parameters.Add("@nombre", SqlDbType.VarChar).Value = this.nombre;
                com.Parameters.Add("@descripcion", SqlDbType.VarChar).Value = this.descripcion;
                com.Parameters.Add("@grupo", SqlDbType.VarChar).Value = this.grupo;
                com.ExecuteNonQuery();
                
                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public Transaccion(BaseContext baseContext, int codigoTransaccion) : base(baseContext)
        {
            try
            {
                base.beginBaseTransaction();

                this.codigoTransaccion = codigoTransaccion;

                SqlCommand com;
                SqlDataReader sqldr;
                
                com = base.getBaseCommand("select * from SEC_Transaccion where codigo_transaccion = @codigo_transaccion;");
                com.Parameters.Add("@codigo_transaccion", SqlDbType.Int).Value = codigoTransaccion;
                sqldr = com.ExecuteReader();
                sqldr.Read();
                this.nombre = (string)sqldr["nombre"];
                this.descripcion = (string)sqldr["descripcion"];
                this.grupo = (string)sqldr["grupo"];
                sqldr.Close();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

    }

}
