using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace passarini.web.business.seguridad
{
    /// <summary>
    /// Esta clase provee funcionalidad de uso com�n para el resto del namespace
    /// </summary>
    public class Util
    {

        public enum CryptoAlgorithm
        {
            SHA, MD5
        }

        /// <summary>
        /// Encripta un string
        /// </summary>
        public static string encriptar(string texto)
        {
            return encriptar(texto, CryptoAlgorithm.SHA);
        }

        /// <summary>
        /// Encripta un string
        /// </summary>
        /// <param name="texto">Texto a encriptar</param>
        /// <param name="algoritmo">Algoritmo de encriptacion (MD5 o SHA512)</param>
        /// <returns></returns>
        public static string encriptar(string texto, CryptoAlgorithm algoritmo)
        {
            if (texto == "")
                return "";

            HashAlgorithm hashAlg = null;

            switch (algoritmo)
            {
                case CryptoAlgorithm.SHA:
                    hashAlg = SHA512.Create();
                    break;
                case CryptoAlgorithm.MD5:
                    hashAlg = MD5.Create();
                    break;
            }

            // Se transforma el texto a bytes
            ASCIIEncoding encoding = new ASCIIEncoding();
            
            // Se calcula el hash
            byte[] hash = hashAlg.ComputeHash(encoding.GetBytes(texto));

            // Finalmente se obtiene el string y se retorna el valor
            return encoding.GetString(hash); 
        }

    }

}
