using System;
using System.Collections.Generic;
using System.Text;
using System.Data; 
using System.Data.SqlClient;
using passarini.web.business.transacciones;

namespace passarini.web.business.seguridad
{

    /// <summary>
    /// Esta clase maneja los conjuntos de Transacciones, Roles y Usuarios.
    /// </summary>
    public class Seguridad : Base
    {
        /// <summary>
        /// Lista los roles de acuerdo a los filtros dados
        /// </summary>
        public DataTable listarRoles()
        {
            return this.listarRoles(-1);
        }

        /// <summary>
        /// Lista los roles de acuerdo a los filtros dados
        /// </summary>
        public DataTable listarRoles(int codigoRol)
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds = new DataSet();
                
                ad = base.getBaseDataAdapter("select * from SEC_Rol where (codigo_rol = @codigo_rol or @codigo_rol < 0) order by nombre;");
                ad.SelectCommand.Parameters.Add("@codigo_rol", SqlDbType.Int).Value = codigoRol;
                ad.Fill(ds, "Tabla");

                base.commitBaseTransaction();

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Obtiene el primer login del usuario asociado al elemento y rol dados
        /// Devuelve "" si no hay asociaci�n
        /// </summary>
        public string obtenerLoginAsociacionDeRol(int codigoRol, int asociadoA)
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds = new DataSet();
                
                ad = base.getBaseDataAdapter("select ur.login from SEC_Usuario_Rol ur where codigo_rol = @codigo_rol and asociado_a = @asociado_a order by ur.login;");
                ad.SelectCommand.Parameters.Add("@codigo_rol", SqlDbType.Int).Value = codigoRol;
                ad.SelectCommand.Parameters.Add("@asociado_a", SqlDbType.Int).Value = asociadoA;
                ad.Fill(ds, "Tabla");

                base.commitBaseTransaction();

                if (ds.Tables[0].Rows.Count == 0)
                {
                    return "";
                }
                else
                {
                    return (String)ds.Tables[0].Rows[0]["login"];
                }
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Registra un rol y devuelve su c�digo
        /// </summary>
        public int registrarRol(string nombre, string descripcion, string queryAsociadoA)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;
                SqlDataReader sqldr;
                int temp;

                com = base.getBaseCommand("insert into SEC_Rol (nombre, descripcion, query_asociado_a) values (@nombre, @descripcion, @query_asociado_a);");
                com.Parameters.Add("@nombre", SqlDbType.VarChar).Value = nombre;
                com.Parameters.Add("@descripcion", SqlDbType.VarChar).Value = descripcion;
                com.Parameters.Add("@query_asociado_a", SqlDbType.VarChar).Value = queryAsociadoA;
                com.ExecuteNonQuery();
                com.CommandText = "select @@identity;";
                com.Parameters.Clear();
                sqldr = com.ExecuteReader();
                sqldr.Read();
                temp = int.Parse(sqldr[0].ToString());
                sqldr.Close();
                
                base.commitBaseTransaction();

                return temp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Elimina el rol dado de la base de datos
        /// </summary>
        public void eliminarRol(int codigoRol)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand();
                com.CommandText = "delete from SEC_Rol where codigo_rol = @codigo_rol;";
                com.Parameters.Add("@codigo_rol", SqlDbType.Int).Value = codigoRol;
                com.ExecuteNonQuery();
                com.CommandText = "delete from SEC_Rol_Transaccion where codigo_rol = @codigo_rol;";
                com.ExecuteNonQuery();
                com.CommandText = "delete from SEC_Usuario_Rol where codigo_rol = @codigo_rol;";
                com.ExecuteNonQuery();
                
                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Lista las transacciones de acuerdo a los filtros dados
        /// </summary>
        public DataTable listarTransacciones(int codigoTransaccion, string grupo)
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds = new DataSet();
                
                ad = base.getBaseDataAdapter("select * from SEC_Transaccion where (codigo_transaccion = @codigo_transaccion or @codigo_transaccion < 0) and (grupo = @grupo or @grupo = '') order by grupo, nombre;");
                ad.SelectCommand.Parameters.Add("@codigo_transaccion", SqlDbType.Int).Value = codigoTransaccion;
                ad.SelectCommand.Parameters.Add("@grupo", SqlDbType.VarChar).Value = grupo;
                ad.Fill(ds, "Tabla");

                base.commitBaseTransaction();

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Lista los distintos grupos a los que pertenecen las transacciones registradas
        /// </summary>
        public DataTable listarGruposTransacciones()
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds = new DataSet();
                
                ad = base.getBaseDataAdapter("select distinct grupo from SEC_Transaccion order by grupo;");
                ad.Fill(ds, "Tabla");
                
                base.commitBaseTransaction();

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Registra una transacci�n y devuelve su c�digo
        /// </summary>
        public int registrarTransaccion(string nombre, string descripcion, string grupo)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;
                SqlDataReader sqldr;
                int temp;

                com = base.getBaseCommand("insert into SEC_Transaccion (Nombre, Descripcion, Grupo) values (@Nombre, @Descripcion, @Grupo);");
                com.Parameters.Add("@nombre", SqlDbType.VarChar).Value = nombre;
                com.Parameters.Add("@descripcion", SqlDbType.VarChar).Value = descripcion;
                com.Parameters.Add("@grupo", SqlDbType.VarChar).Value = grupo;
                com.ExecuteNonQuery();
                com.CommandText = "select @@identity;";
                com.Parameters.Clear();
                sqldr = com.ExecuteReader();
                sqldr.Read();
                temp = int.Parse(sqldr[0].ToString());
                sqldr.Close();

                base.commitBaseTransaction();

                return temp;
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Elimina la transacci�n dada, s�lo si no ha sido referenciada
        /// </summary>
        public void eliminarTransaccion(int codigoTransaccion)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand();
                com.CommandText = "delete from SEC_Transaccion where codigo_transaccion = @codigo_transaccion and 0 = (select count(*) from sec_bitacora where codigo_transaccion = @codigo_transaccion);";
                com.Parameters.Add("@codigo_transaccion", SqlDbType.Int).Value = codigoTransaccion;
                if (com.ExecuteNonQuery() == 0)
                {
                    throw new Exception("La transacci�n a eliminar no existe, o est� siendo referenciada en la bit�cora.");
                }
                com.CommandText = "delete from SEC_Rol_Transaccion where codigo_transaccion = @codigo_transaccion;";
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Lista los usuarios de acuerdo a los filtros dados
        /// </summary>
        public DataTable listarUsuarios(string login)
        {
            return this.listarUsuarios(login, "");
        }

        /// <summary>
        /// Lista los usuarios de acuerdo a los filtros dados
        /// </summary>
        public DataTable listarUsuarios(string login, string nombre)
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds = new DataSet();

                ad = base.getBaseDataAdapter("select * from SEC_Usuario where (login = @login or @login = '') and (nombre like '%' + @nombre + '%' or @nombre = '') order by login;");
                ad.SelectCommand.Parameters.Add("@login", SqlDbType.VarChar).Value = login;
                ad.SelectCommand.Parameters.Add("@nombre", SqlDbType.VarChar).Value = nombre;
                ad.Fill(ds, "Tabla");

                base.commitBaseTransaction();

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Lista los usuarios de acuerdo a los filtros dados
        /// </summary>
        public DataTable listarUsuariosAgente(string login, string nombre)
        {
            try
            {
                base.beginBaseTransaction();

                SqlDataAdapter ad;
                DataSet ds = new DataSet();

                ad = base.getBaseDataAdapter("select * from SEC_Usuario where (login = @login or @login = '') and (nombre like '%' + @nombre + '%' or @nombre = '') and (login in(select login from sec_usuario_rol  where codigo_rol='2' ))  order by login;");
                ad.SelectCommand.Parameters.Add("@login", SqlDbType.VarChar).Value = login;
                ad.SelectCommand.Parameters.Add("@nombre", SqlDbType.VarChar).Value = nombre;                
                ad.Fill(ds, "Tabla");

                base.commitBaseTransaction();

                return ds.Tables[0];
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }
        
        /// <summary>
        /// Registra un usuario
        /// </summary>
        public void registrarUsuario(string login, string nombre, string descripcion, string email, string observaciones, string password, string preguntaOlvidoPassword, string respuestaOlvidoPassword)
        {
            try
            {

                base.beginBaseTransaction();

                SqlCommand com;
                
                com = base.getBaseCommand("insert into SEC_Usuario (Login, Nombre, Descripcion, Email, Observaciones, Activo, Password, Pregunta_Olvido_Password, Respuesta_Olvido_Password, salt) values (@Login, @Nombre, @Descripcion, @Email, @Observaciones, @Activo, @Password, @Pregunta_Olvido_Password, @Respuesta_Olvido_Password, '');");
                com.Parameters.Add("@login", SqlDbType.VarChar).Value = login;
                com.Parameters.Add("@nombre", SqlDbType.VarChar).Value = nombre;
                com.Parameters.Add("@descripcion", SqlDbType.VarChar).Value = descripcion;
                com.Parameters.Add("@email", SqlDbType.VarChar).Value = email;
                com.Parameters.Add("@observaciones", SqlDbType.Text).Value = observaciones;
                com.Parameters.Add("@activo", SqlDbType.Bit).Value = true;
                com.Parameters.Add("@password", SqlDbType.VarChar).Value = Util.encriptar(password);
                com.Parameters.Add("@pregunta_olvido_password", SqlDbType.VarChar).Value = preguntaOlvidoPassword;
                com.Parameters.Add("@respuesta_olvido_password", SqlDbType.VarChar).Value = Util.encriptar(respuestaOlvidoPassword);
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Elimina un usuario, s�lo si �ste no ha sido referenciado
        /// </summary>
        public void eliminarUsuario(string login)
        {
            try
            {
                base.beginBaseTransaction();

                SqlCommand com;

                com = base.getBaseCommand();
                com.CommandText = "delete from SEC_Usuario where login = @login;";
                com.Parameters.Add("@login", SqlDbType.VarChar).Value = login;
                if (com.ExecuteNonQuery() == 0)
                {
                    throw new Exception("El usuario a eliminar no existe, o est� siendo referenciado en otras tablas.");
                }
                com.CommandText = "delete from SEC_Usuario_Rol where login = @login;";
                com.ExecuteNonQuery();

                base.commitBaseTransaction();
            }
            catch (Exception ex)
            {
                base.rollbackBaseTransaction();
                throw ex;
            }
        }

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        public Seguridad(BaseContext baseContext)
            : base(baseContext)
        {
        }

    }
}
