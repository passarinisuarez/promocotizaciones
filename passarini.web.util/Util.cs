using System;

namespace passarini.web.util
{
	/// <summary>
	/// Clases de utilidad general para la aplicación
	/// </summary>
	public class Util
	{
		/// <summary>
		/// Dada una hora local del servidor, devuelve dicha hora convertida a GMT-4.30 (Venezuela)
		/// </summary>
		public static DateTime LocalToVenezuela(DateTime d)
		{
			return d.ToUniversalTime().AddHours(-4).AddMinutes(-30);
		}

		/// <summary>
		/// Dada una hora GMT-4.30 (Venezuela), devuelve dicha hora convertida a la hora local del servidor
		/// </summary>
		public static DateTime VenezuelaToLocal(DateTime d) 
		{
			return d.AddHours(4).AddMinutes(30).ToLocalTime();
		}
	}
}
