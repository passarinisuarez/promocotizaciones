using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace passarini.web.business.transacciones
{
    /// <summary>
    /// Esta clase es la base de la cu�l heredan todas las dem�s clases que hagan uso de transacciones de base de datos
    /// </summary>
    public class Base
    {

        /// <summary>
        /// Contexto que se propaga a todas las dem�s clases de FactorAG instanciadas
        /// </summary>
        private BaseContext baseContext;
        /// <summary>
        /// Se usa para conocer si una excepcion ha sido registrada en el sistema
        /// </summary>
        private int exceptionCode = 0;

        /// <summary>
        /// Constructor de la clase. Recibe el contexto bajo el cu�l va a operar, con la conexi�n
        /// a base de datos, etc.
        /// </summary>
        public Base(BaseContext baseContext)
        {
            this.baseContext = baseContext;
        }

        /// <summary>
        /// Devuelve una conexi�n abierta a la base de datos
        /// Si el contexto se encuentra dentro de una transacci�n de base de datos abierta, devuelve 
        /// dicha conexi�n ya abierta.
        /// En caso contrario, se crea una nueva conexi�n y transacci�n
        /// </summary>
        public void beginBaseTransaction()
        {
            if (baseContext.baseTransactionNestingLevel == 0)
            {
                baseContext.baseConnection = new SqlConnection(baseContext.baseConnectionString);
                baseContext.baseConnection.Open();
                baseContext.baseTransaction = baseContext.baseConnection.BeginTransaction(IsolationLevel.Serializable);
            }
            baseContext.baseTransactionNestingLevel++;
        }

        /// <summary>
        /// Cierra la conexi�n abierta a base de datos
        /// Si el contexto se encontraba dentro de una transacci�n de base de datos abierta,
        /// no la cierra, sino que prepara el camino para un retorno a la llamada anterior en la pila
        /// En caso contrario, cierra la conexi�n
        /// </summary>
        public void commitBaseTransaction()
        {
            if (baseContext.baseTransactionNestingLevel > 0)
            {
                baseContext.baseTransactionNestingLevel--;
            }
            if (baseContext.baseTransactionNestingLevel == 0)
            {
                baseContext.baseTransaction.Commit();
                baseContext.baseConnection.Close();
                baseContext.baseConnection = null;
                baseContext.baseTransaction = null;
            }
        }

        /// <summary>
        /// Hace rollback a la transacci�n del contexto actual y cierra la conexi�n
        /// </summary>
        public void rollbackBaseTransaction()
        {
            if (this.exceptionCode == 0)
            {
                try
                {
                    // Se registra el error en la base de datos

                }
                catch
                {

                }
            }
            if (baseContext.baseTransactionNestingLevel > 0)
            {
                baseContext.baseTransactionNestingLevel = 0;
            }
            if (baseContext.baseTransaction == null)
            {
                return;
            }
            try
            {
                baseContext.baseTransaction.Rollback();
                baseContext.baseConnection.Close();
            }
            catch
            {
            }
            baseContext.baseConnection = null;
            baseContext.baseTransaction = null;
        }

        /// <summary>
        /// Devuelve la conexi�n de base de datos abierta en el contexto actual
        /// En caso de que no haya una conexi�n abierta, retorna una excepci�n
        /// </summary>
        public SqlConnection getBaseConnection()
        {
            if (baseContext.baseTransaction == null)
            {
                throw new Exception("Para llamar a getBaseConnection() debe llamar primeramente a beginBaseTransaction()");
            }
            return baseContext.baseConnection;
        }

        /// <summary>
        /// Devuelve la transacci�n de base de datos abierta en el contexto actual
        /// En caso de que no haya una conexi�n/transacci�n abierta, retorna una excepci�n
        /// </summary>
        public SqlTransaction getBaseTransaction()
        {
            if (baseContext.baseTransaction == null)
            {
                throw new Exception("Para llamar a getBaseTransaction() debe llamar primeramente a beginBaseTransaction()");
            }
            return baseContext.baseTransaction;
        }

        /// <summary>
        /// Devuelve un objeto SqlCommand para la conexi�n/transacci�n de base de datos abierta en el contexto actual
        /// En caso de que no haya una conexi�n/transacci�n abierta, retorna una excepci�n
        /// </summary>
        public SqlCommand getBaseCommand(String cmdText)
        {
            if (baseContext.baseTransaction == null)
            {
                throw new Exception("Para llamar a getCommand() debe llamar primeramente a beginBaseTransaction()");
            }
            SqlCommand com = new SqlCommand(cmdText, baseContext.baseConnection);
            com.Transaction = baseContext.baseTransaction;

            return com;
        }

        /// <summary>
        /// Devuelve un objeto SqlCommand para la conexi�n/transacci�n de base de datos abierta en el contexto actual
        /// En caso de que no haya una conexi�n/transacci�n abierta, retorna una excepci�n
        /// </summary>
        public SqlCommand getBaseCommand()
        {
            return this.getBaseCommand("");
        }

        /// <summary>
        /// Devuelve un objeto SqlDataAdapter para la conexi�n/transacci�n de base de datos abierta en el contexto actual
        /// En caso de que no haya una conexi�n/transacci�n abierta, retorna una excepci�n
        /// </summary>
        public SqlDataAdapter getBaseDataAdapter(String selectCommandText)
        {
            if (baseContext.baseTransaction == null)
            {
                throw new Exception("Para llamar a getDataAdapter() debe llamar primeramente a beginBaseTransaction()");
            }

            SqlDataAdapter ad = new SqlDataAdapter(selectCommandText, baseContext.baseConnection);
            ad.SelectCommand.Transaction = baseContext.baseTransaction;

            return ad;
        }

        /// <summary>
        /// Devuelve un objeto SqlDataAdapter para la conexi�n/transacci�n de base de datos abierta en el contexto actual
        /// En caso de que no haya una conexi�n/transacci�n abierta, retorna una excepci�n
        /// </summary>
        public SqlDataAdapter getBaseDataAdapter()
        {
            return this.getBaseDataAdapter("");
        }

        /// <summary>
        /// Devuelve el contexto actual
        /// </summary>
        /// <returns></returns>
        public BaseContext getBaseContext()
        {
            return this.baseContext;
        }

        /// <summary>
        /// Devuelve un nuevo contexto en base al contexto actual, para ser usado en una nueva instancia
        /// </summary>
        public BaseContext getBaseContextForNewInstance()
        {
            return new BaseContext(this.baseContext);
        }

        /// <summary>
        /// Devuelve un contexto para ser utilizado en una instancia devuelta en un m�todo
        /// </summary>
        public BaseContext getBaseContextForReturnedInstance()
        {
            //if (baseContext.baseTransactionNestingLevel <= 1)
            //{
            return new BaseContext(baseContext.baseConnectionString);
            //}
            //else
            //{
            //    return this.getBaseContextForNewInstance();
            //}
        }

        /// <summary>
        /// Extrae mediante reflexion los atributos de el objeto para registrarlo en la base de datos
        /// </summary>
        /// <returns></returns>
        public string getObjectInfo()
        {
            System.Type type = this.GetType();

            string objectInfo = string.Format("DESCRIPCION DE DATOS EN {0}\n\n", type.Name);

            System.Reflection.PropertyInfo[] props = type.GetProperties();
            foreach (System.Reflection.PropertyInfo info in props)
            {
                try
                {
                    objectInfo += string.Format("\t{0} -> {1}\n", info.Name, info.GetValue(this, null));
                }
                catch { }
            }

            return objectInfo;
        }

#if DEBUG
        /// <summary>
        /// Solo debe existir en debug, es para pruebas
        /// </summary>
        /// <returns></returns>
        public string RegistrarException()
        {
            return this.getObjectInfo();
        }
#endif
    }
}
