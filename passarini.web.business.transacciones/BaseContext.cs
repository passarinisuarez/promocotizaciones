using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace passarini.web.business.transacciones
{
    /// <summary>
    /// Contexto que incluye variables requeridas para cualquier instancia que herede de Base, entre
    /// otros, conexi�n a la base de datos, transacci�n abierta, etc.
    /// </summary>
    public class BaseContext
    {
        /// <summary>
        /// String de conexi�n a la base de datos
        /// </summary>
        internal String baseConnectionString;

        /// <summary>
        /// Conexi�n abierta a la base de datos
        /// </summary>
        internal SqlConnection baseConnection;

        /// <summary>
        /// Nivel de anidamiento de llamadas dentro de la misma instancia
        /// </summary>
        internal int baseTransactionNestingLevel;

        /// <summary>
        /// Transacci�n de base de datos abierta
        /// </summary>
        internal SqlTransaction baseTransaction;

        /// <summary>
        /// Constructor para una nueva transacci�n
        /// S�lo recibe el string de conexi�n a base de datos
        /// </summary>
        public BaseContext(String baseConnectionString)
        {
            this.baseConnectionString = baseConnectionString;
            this.baseConnection = null;
            this.baseTransaction = null;
            this.baseTransactionNestingLevel = 0;
        }

        /// <summary>
        /// Constructor para una transacci�n ya existente
        /// Recibe una instancia de BaseContext ya creada
        /// </summary>
        internal BaseContext(BaseContext baseContext)
        {
            this.baseConnectionString = baseContext.baseConnectionString;
            this.baseConnection = baseContext.baseConnection;
            this.baseTransaction = baseContext.baseTransaction;
#warning Esto creo que va a causar un error porque esta creando una nueva instancia con un nivel ya preestablecido, puede causar bloqueos si se usa incorrectamente, revisar de todas formas

            this.baseTransactionNestingLevel = baseContext.baseTransactionNestingLevel + 1;
        }
    }
}
