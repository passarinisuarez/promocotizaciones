$('#formNuevaPromo').validator()

$('#formEditarPromo').validator()

$('#formAgregarTextoLibre').validator()

$('#formEditarTarifa').validator()

$('#formDetalleTarifa').validator()

$('#formCamposDeTarifas').validator()

$('#formLogin').validator()

$('#formCrearAsesor').validator()

$('#formEditarAsesor').validator()

$('#formEnviarEmail').validator()

$('#formCotizacion').validator()


/**Funcion para validar el formulario de Promociones (formEditarPromo)
$(document).ready(function () {
    $('#formEditarPromo').bootstrapValidator({
        container: '#messages',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'titulo': {
                validators: {
                    notEmpty: {
                        message: 'El Titulo es requerido, no puede ser vacio.'
                    }
                }
            },
            'orden': {
                validators: {
                    numeric: {
                        message: 'El numero de Orden debe ser solo digitos.'
                    }
                }
            },
            'operador': {
                validators: {
                    notEmpty: {
                        message: 'El Operador es requerido, no puede ser vacio.'
                    }
                }
            }
        }
    });
});

/**Funcion para validar el formulario de Promociones (formAgregarTextoLibre)
$(document).ready(function () {
    $('#formAgregarTextoLibre').bootstrapValidator({
        container: '#texto_libre_validacion',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            textoLibre: {
                validators: {
                    notEmpty: {
                        message: 'El Titulo es requerido, no puede ser vacio.'
                    }
                }
            }
        }
    });
});

/**Funcion para validar el formulario de Cotizaciones (formEditarTarifa)
$(document).ready(function () {
    $('#formEditarTarifa').bootstrapValidator({
        container: '#validacion_tarifas',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'tarifa_concepto': {
                validators: {
                    notEmpty: {
                        message: 'El Concepto es requerido, no puede ser vacio.'
                    }
                }
            },
            'tarifa_monto': {
                validators: {
                    regexp: {
                        regexp: /^\s*?([\d]+(\,\d{1,2})?|\,\d{1,2})\s*$/,
                        message: 'El monto debe ser digitos y de la forma 12345,67.'
                    },
                    notEmpty: {
                        message: 'El Monto es requerido, no puede ser vacio.'
                    }
                }
            }
        }
    });
});

/**Funcion para validar el formulario de Promociones (formDetalleTarifa)
$(document).ready(function () {
    $('#formDetalleTarifa').bootstrapValidator({
        container: '#validacion_tarifas',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'tarifa_descripcion': {
                validators: {
                    notEmpty: {
                        message: 'El Concepto es requerido, no puede ser vacio.'
                    }
                }
            },
            'tarifa_monto': {
                validators: {
                    regexp: {
                        regexp: /^\s*?([\d]+(\,\d{1,2})?|\,\d{1,2})\s*$/,
                        message: 'El monto debe ser digitos y de la forma 12345,67.'
                    },
                    notEmpty: {
                        message: 'El Monto es requerido, no puede ser vacio.'
                    }
                }
            }
        }
    });
});

/**Funcion para validar el formulario de Promociones (formCamposDeTarifas)
$(document).ready(function () {
    $('#formCamposDeTarifas').bootstrapValidator({
        container: '#validacion_campo_datos_tarifa',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            'tarifa_tipo': {
                validators: {
                    notEmpty: {
                        message: 'El Tipo de Tarifa es requerido, no puede ser vacio.'
                    }
                }
            }
        }
    });
});

/**Funcion para validar el formulario de Login (formLogin)
$(document).ready(function () {
    $('#formLogin').bootstrapValidator({
        container: '#messages',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            usuario: {
                validators: {
                    notEmpty: {
                        message: 'El Usuario es requerido, no puede ser vacio.'
                    }
                }
            },
            contrasena: {
                validators: {

                }
            }
        }
    });
});
**/
function formEditarPromocion() {
    var desde = document.getElementById("desde").value;
    var hasta = document.getElementById("hasta").value;

    if (desde == "" || (/^\s+$/.test(desde))) {
        return false;
    }

    if (hasta == "" || (/^\s+$/.test(hasta))) {
        return false;
    }    

    document.forms["formEditarPromocion"].submit();
}

function formNuevaPromo() {
    var desde = document.getElementById("desde").value;
    var hasta = document.getElementById("hasta").value;
    var publicacion_desde = document.getElementById("publicacion_desde").value;
    var publicacion_hasta = document.getElementById("publicacion_hasta").value;

    if (desde == "" || (/^\s+$/.test(desde))) {
        return false;
    }

    if (hasta == "" || (/^\s+$/.test(hasta))) {
        return false;
    }

    if (publicacion_desde == "" || (/^\s+$/.test(publicacion_desde))) {
        return false;
    }

    if (publicacion_hasta == "" || (/^\s+$/.test(publicacion_hasta))) {
        return false;
    }

    document.forms["formNuevaPromo"].submit();
}

function formCamposTarifaPromo() {
    var tarifa = document.getElementById("tarifa").value;
    var tarifa_tipo = document.getElementById("tarifa_tipo").value;

    if (tarifa == "" || (/^\s+$/.test(tarifa))) {
        alert("El campo de Tarifa no puede ser vacio.");
        return false;
    }

    
    if (!/^\s*?([\d]+(\,\d{1,2})?|\,\d{1,2})\s*$/.test(tarifa)) {
        alert("El campo de Tarifa debe ser digitos y decimales. De la forma 12345,67.");
        return false;
    }

    if (tarifa_tipo == "" || (/^\s+$/.test(tarifa_tipo))) {
        alert("El campo de Tipo de Tarifa no puede ser vacio.");
        return false;
    }

    document.forms["formCamposDeTarifas"].submit();
}

function formDescripcion() {
    document.forms["formDescripcion"].submit();
}


function formPromocionDescripcion() {

    document.forms["formPromocionDescripcion"].submit();
}

function formPDF(campoTexto, valor, form) {
    document.getElementById(campoTexto).value = valor;
    document.forms[form].submit();
}

function mostrarCampos(ocultarbtn, mostrarForm) {

    document.getElementById(ocultarbtn).style.display = 'none';
    document.getElementById(mostrarForm).style.display = '';

}

function mostrarCamposEmail(ocultarbtn, mostrarForm, campoTexto, value) {

    document.getElementById(campoTexto).value = value;
    document.getElementById(ocultarbtn).style.display = 'none';
    document.getElementById(mostrarForm).style.display = '';

}

function cancelarCampos2(mostrarbtn, ocultarForm, campoTexto) {

    document.getElementById(mostrarbtn).style.display = '';
    document.getElementById(ocultarForm).style.display = 'none';
    document.getElementById(campoTexto).value = '';

}

function mostrarCargando(ocultar, mostrar, form) {

    document.getElementById(ocultar).disabled = true;
    document.getElementById(ocultar).style.display = 'none';
    document.getElementById(mostrar).style.display = '';

    document.forms[form].submit();

}

function ocultar(campotexto) {
    document.getElementById(campotexto).style.display = 'none';
}

function mostrar(campotexto) {
    document.getElementById(campotexto).style.display = '';
}

function vaciar(campotexto) {
    document.getElementById(campotexto).value = '';
}

function setValorById(campotexto, valor) {
    document.getElementById(campotexto).value = valor;
}

function setActiveTypePromo(viaje) {

    if (viaje == 1) {
        $('#viaje').addClass("active-tipo")
        $('#general').removeClass("active-tipo")
    } else {
        $('#general').addClass("active-tipo")
        $('#viaje').removeClass("active-tipo")
    }
    
}

function chageFavorite() {
    var fav = document.getElementById('destacado').value
    
    if (fav == 1) {
        setValorById('destacado', 0)
        $('#star-icon').removeClass("fa-star")
        $('#star-icon').addClass("fa-star-o")
    } else {
        setValorById('destacado', 1)
        $('#star-icon').addClass("fa-star")
        $('#star-icon').removeClass("fa-star-o")
    }

}


