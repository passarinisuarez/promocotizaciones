﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using passarini.web.business.transacciones;
using PromoCotizaciones.business;

namespace Promocotizaciones
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            String usuario = "";
            try
            {
                usuario = (String)Session["usuario"];
            }
            catch { }

            String mensaje = "\r\n\r\nHa ocurrido un error en el sistema, generado por:" + usuario
                + "\r\n " + Request.UserHostAddress.ToString()
                + "\r\n " + Request.Url.ToString()
                + "\r\n" + Server.GetLastError().ToString();

            try
            {
                String logPath = (String)Application["LogPath"];
                StreamWriter logFile = new StreamWriter(logPath, true);
                logFile.WriteLine(mensaje);
                logFile.Close();
            }
            catch
            {
            }

            // envía el error por email al administrador
            try
            {
                BaseContext ctx = new BaseContext(System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString_passarini"].ConnectionString);
                Auxiliares auxiliares = new Auxiliares(ctx);
                String to = auxiliares.obtenerValorParametro("email_administrador");
                auxiliares.enviarEmail(to, "", "Error en la aplicación", mensaje);
            }
            catch
            {
            }

            // redirecciona al errorpage
            if (!Request.Url.ToString().ToLower().EndsWith("Error"))
            {
                if (Session["usuario"] != null)
                {
                    string empresa = (String)Session["nombreEmpresa"];
                    Session["error"] = Server.GetLastError().ToString();
                    Response.Redirect("/" + empresa + "/Error", true);
                }
                else {
                    Response.Redirect("/Error", true);
                }
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}