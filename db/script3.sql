-- script temporal por error que da cuando no hay promociones existentes

INSERT [dbo].[Pro_Promocion] ([Codigo_Promocion], [Codigo_Clasificacion], [Codigo_Subclasificacion], [Grupo], [Orden], [Tarifa_Consolidada_Moneda], [Tarifa_Consolidada], [Tarifa_Consolidada_Tipo], [Titulo], [Subtitulo], [Valida_Desde], [Valida_Hasta], [Publicacion_Desde], [Publicacion_Hasta], [Activa], [Modificada], [SEO_URL], [Color_Titulos], [Link], [Destacado], [Tipo]) VALUES (2, 2, 3, N'', 1, N'Bs', 25580, N'Desde', N'Tucacas - Bahía Kangrejo ', N'Los mejores precios ', CAST(0xA59C0000 AS SmallDateTime), CAST(0xA5BB0000 AS SmallDateTime), CAST(0xA59C0000 AS SmallDateTime), CAST(0xA6EE0000 AS SmallDateTime), 0, N'0', N'promo2', N'white', N'', 0, 2)
GO
INSERT [dbo].[Pro_Promocion_Tarifa] ([Codigo_Promocion], [Codigo_Moneda], [Descripcion], [Monto], [Orden_Tarifa]) VALUES (2, N'Bs                       ', N'Hotel Coche Paradise ', 136615, 6)
GO
INSERT [dbo].[Pro_Promocion_Texto_Libre] ([Codigo_Promocion], [Titulo_Texto_Libre], [Texto_Libre], [Orden]) VALUES (2, N'Condiciones', N'<ul><li>No se acepta menores de 4 años</li></ul>', 1)
GO
INSERT [dbo].[Pro_Promocion_Viaje] ([Codigo_Promocion], [Operador], [Codigo_Asesor]) VALUES (2, N'admin', 1)
GO
