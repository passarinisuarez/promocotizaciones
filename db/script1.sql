GO
/****** Object:  Table [dbo].[Aux_Agente]    Script Date: 29/1/2017 11:20:56 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Aux_Agente](
	[Codigo_Agente] [numeric](18, 0) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Codigo_Agente_Supervisor] [numeric](18, 0) NULL,
	[Activo] [bit] NOT NULL,
	[Codigo_Sucursal] [numeric](18, 0) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Aux_Asesor]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Aux_Asesor](
	[Codigo_Asesor] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Supervisor] [bit] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Aux_Cliente]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Aux_Cliente](
	[Codigo_Cliente] [numeric](18, 0) NOT NULL,
	[Codigo_Agente] [numeric](18, 0) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Solo_Supervisores_Ven_Todas_Las_Solicitudes] [bit] NOT NULL,
	[Activo] [bit] NOT NULL,
	[Codigo_Sucursal] [numeric](18, 0) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Aux_Contacto]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Aux_Contacto](
	[Codigo_Contacto] [int] NOT NULL,
	[Fecha_Hora] [datetime] NOT NULL,
	[Asociado_A_Tipo] [int] NULL,
	[Asociado_A] [int] NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[IP] [varchar](50) NOT NULL,
	[Mensaje] [text] NOT NULL,
	[Atendido] [bit] NOT NULL,
	[Codigo_Departamento] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Aux_Empresa]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Aux_Empresa](
	[Codigo_Empresa] [varchar](15) NOT NULL,
	[Nombre_Empresa] [varchar](120) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Aux_IATA_Aeropuerto]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Aux_IATA_Aeropuerto](
	[Codigo_Aeropuerto] [varchar](50) NOT NULL,
	[Codigo_Pais] [varchar](50) NOT NULL,
	[Ciudad_Nombre] [varchar](100) NOT NULL,
	[Pais_Nombre] [varchar](100) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Aux_Parametro]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Aux_Parametro](
	[Codigo_Parametro] [varchar](50) NOT NULL,
	[Valor] [varchar](250) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Aux_Sucursal]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Aux_Sucursal](
	[Codigo_Sucursal] [numeric](18, 0) NOT NULL,
	[Nombre] [varchar](50) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cot_Cotizacion]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cot_Cotizacion](
	[Codigo_Cotizacion] [int] NOT NULL,
	[Codigo_Promocion] [int] NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[IP] [varchar](50) NULL,
	[Texto_Solicitud] [text] NULL,
	[Via] [char](1) NOT NULL,
	[Status] [char](1) NOT NULL,
	[Numero_Adultos] [int] NOT NULL,
	[Numero_Ninos] [int] NOT NULL,
	[Numero_Infantes] [int] NOT NULL,
	[Codigo_Asesor] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cot_Cotizacion_Detalle_Tarifa]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cot_Cotizacion_Detalle_Tarifa](
	[Codigo_Cotizacion] [int] NOT NULL,
	[Concepto] [varchar](50) NOT NULL,
	[Moneda] [varchar](50) NOT NULL,
	[Monto] [float] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cot_Cotizacion_Status]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cot_Cotizacion_Status](
	[Codigo_Cotizacion] [int] NOT NULL,
	[Fecha_Hora] [datetime] NOT NULL,
	[Status] [char](1) NOT NULL,
	[Codigo_Asesor] [bit] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cot_Cotizacion_Texto_Libre]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cot_Cotizacion_Texto_Libre](
	[Codigo_Cotizacion] [int] NOT NULL,
	[Titulo_Texto_Libre] [varchar](25) NOT NULL,
	[Texto_Libre] [text] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Gal_Fotos]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Gal_Fotos](
	[Codigo_Galeria] [int] NOT NULL,
	[Orden] [int] NOT NULL,
	[URL] [varchar](500) NOT NULL,
	[Titulo] [varchar](50) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Gal_Galeria]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Gal_Galeria](
	[Codigo_Galeria] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pro_Promocion]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pro_Promocion](
	[Codigo_Promocion] [int] NOT NULL,
	[Codigo_Clasificacion] [int] NOT NULL,
	[Codigo_Subclasificacion] [int] NULL,
	[Grupo] [varchar](50) NOT NULL,
	[Orden] [int] NULL,
	[Tarifa_Consolidada_Moneda] [varchar](10) NULL,
	[Tarifa_Consolidada] [float] NULL,
	[Tarifa_Consolidada_Tipo] [varchar](20) NOT NULL,
	[Titulo] [varchar](100) NULL,
	[Subtitulo] [varchar](100) NOT NULL,
	[Valida_Desde] [smalldatetime] NOT NULL,
	[Valida_Hasta] [smalldatetime] NOT NULL,
	[Publicacion_Desde] [smalldatetime] NOT NULL,
	[Publicacion_Hasta] [smalldatetime] NOT NULL,
	[Activa] [bit] NOT NULL,
	[Modificada] [char](1) NOT NULL,
	[SEO_URL] [varchar](100) NOT NULL,
	[Color_Titulos] [varchar](20) NOT NULL,
	[Link] [text] NOT NULL,
	[Destacado] [bit] NOT NULL,
	[Tipo] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pro_Promocion_Tarifa]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pro_Promocion_Tarifa](
	[Codigo_Promocion] [int] NOT NULL,
	[Codigo_Moneda] [char](25) NOT NULL,
	[Descripcion] [varchar](250) NOT NULL,
	[Monto] [float] NOT NULL,
	[Orden_Tarifa] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pro_Promocion_Texto_Libre]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pro_Promocion_Texto_Libre](
	[Codigo_Promocion] [int] NOT NULL,
	[Titulo_Texto_Libre] [varchar](25) NOT NULL,
	[Texto_Libre] [text] NOT NULL,
	[Orden] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pro_Promocion_Viaje]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pro_Promocion_Viaje](
	[Codigo_Promocion] [int] NOT NULL,
	[Operador] [varchar](50) NOT NULL,
	[Codigo_Asesor] [int] NOT NULL,
 CONSTRAINT [PK_Pro_Promocion_Viaje] PRIMARY KEY CLUSTERED 
(
	[Codigo_Promocion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sbl_Cliente_Aerolinea_Frecuente]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sbl_Cliente_Aerolinea_Frecuente](
	[Codigo_Cliente] [numeric](18, 0) NOT NULL,
	[Aerolinea_Nombre] [varchar](100) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sbl_Cliente_Destino_Frecuente]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sbl_Cliente_Destino_Frecuente](
	[Codigo_Cliente] [numeric](18, 0) NOT NULL,
	[Destino_Nombre] [varchar](100) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sbl_Cliente_Pasajero_Frecuente]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sbl_Cliente_Pasajero_Frecuente](
	[Codigo_Cliente] [numeric](18, 0) NOT NULL,
	[Pasajero_Nombre] [varchar](100) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sbl_Solicitud]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sbl_Solicitud](
	[Codigo_Solicitud] [numeric](18, 0) NOT NULL,
	[Codigo_Cliente] [numeric](18, 0) NOT NULL,
	[Codigo_Agente_Asignado] [numeric](18, 0) NOT NULL,
	[Cliente_Nombre] [varchar](100) NOT NULL,
	[Solicitud_Fecha_Hora] [datetime] NOT NULL,
	[Facturar_A] [varchar](100) NOT NULL,
	[Status] [char](1) NOT NULL,
	[Emision_Fecha_Hora_Limite] [datetime] NULL,
	[Codigo_Sucursal] [numeric](18, 0) NOT NULL,
	[Hotel] [varchar](50) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sbl_Solicitud_Detalle]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sbl_Solicitud_Detalle](
	[Codigo_Solicitud_Detalle] [numeric](18, 0) NOT NULL,
	[Codigo_Solicitud] [numeric](18, 0) NOT NULL,
	[Factura_Nro] [varchar](50) NOT NULL,
	[Localizador] [varchar](50) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sbl_Solicitud_Detalle_Observacion]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sbl_Solicitud_Detalle_Observacion](
	[Codigo_Solicitud_Detalle] [numeric](18, 0) NOT NULL,
	[Codigo_Solicitud] [numeric](18, 0) NOT NULL,
	[Fecha_Hora] [datetime] NOT NULL,
	[Agente_Cliente] [char](1) NOT NULL,
	[Codigo_Agente] [numeric](18, 0) NULL,
	[Observacion] [text] NOT NULL,
	[Login] [varchar](50) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sbl_Solicitud_Detalle_Pasajero]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sbl_Solicitud_Detalle_Pasajero](
	[Codigo_Solicitud_Detalle] [numeric](18, 0) NOT NULL,
	[Codigo_Solicitud] [numeric](18, 0) NOT NULL,
	[Pasajero_Nombre] [varchar](100) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sbl_Solicitud_Detalle_Tramo]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sbl_Solicitud_Detalle_Tramo](
	[Codigo_Solicitud_Detalle] [numeric](18, 0) NOT NULL,
	[Codigo_Solicitud] [numeric](18, 0) NOT NULL,
	[Fecha_Hora] [datetime] NOT NULL,
	[Origen] [varchar](50) NOT NULL,
	[Destino] [varchar](50) NOT NULL,
	[Aerolinea] [varchar](50) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sbl_Solicitud_Observacion]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sbl_Solicitud_Observacion](
	[Codigo_Solicitud] [varchar](50) NOT NULL,
	[Fecha_Hora] [datetime] NOT NULL,
	[Agente_Cliente] [char](1) NOT NULL,
	[Codigo_Agente] [numeric](18, 0) NULL,
	[Observacion] [text] NOT NULL,
	[Login] [varchar](50) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sbl_Solicitud_Status]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sbl_Solicitud_Status](
	[Codigo_Solicitud] [numeric](18, 0) NOT NULL,
	[Fecha_Hora] [datetime] NOT NULL,
	[Status] [char](1) NOT NULL,
	[Login] [varchar](50) NOT NULL,
	[Horas_Desde_Status_Anterior] [int] NOT NULL,
	[Minutos_Desde_Status_Anterior] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sec_Bitacora]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sec_Bitacora](
	[Fecha_Hora] [datetime] NOT NULL,
	[Login] [varchar](50) NOT NULL,
	[Codigo_Transaccion] [int] NOT NULL,
	[Observaciones] [text] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sec_Rol]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sec_Rol](
	[Codigo_Rol] [int] NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Descripcion] [varchar](250) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sec_Rol_Transaccion]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sec_Rol_Transaccion](
	[Codigo_Rol] [int] NOT NULL,
	[Codigo_Transaccion] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sec_Transaccion]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sec_Transaccion](
	[Codigo_Transaccion] [int] NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Descripcion] [varchar](250) NOT NULL,
	[Grupo] [varchar](50) NOT NULL,
	[Requiere_Confirmacion] [bit] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sec_Usuario]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sec_Usuario](
	[Login] [varchar](50) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Descripcion] [varchar](250) NOT NULL,
	[Email] [varchar](100) NOT NULL,
	[Observaciones] [text] NOT NULL,
	[Activo] [bit] NOT NULL,
	[Password] [varchar](512) NOT NULL,
	[Pregunta_Olvido_Password] [varchar](512) NOT NULL,
	[Respuesta_Olvido_Password] [varchar](512) NOT NULL,
	[salt] [varchar](50) NOT NULL,
	[codigo_recordatorio] [varchar](100) NULL,
	[codigo_recordatorio_encriptado] [varchar](512) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sec_Usuario_Rol]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sec_Usuario_Rol](
	[Login] [varchar](50) NOT NULL,
	[Codigo_Rol] [int] NOT NULL,
	[Asociado_A] [numeric](18, 0) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tip_Tip_Clasificacion]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tip_Tip_Clasificacion](
	[Codigo_Tip_Clasificacion] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tip_Tip_Viajero]    Script Date: 29/1/2017 11:21:00 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tip_Tip_Viajero](
	[Codigo_Tip_Viajero] [int] NOT NULL,
	[Codigo_Tip_Clasificacion] [int] NOT NULL,
	[Titulo] [varchar](100) NOT NULL,
	[Texto] [text] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Pro_Promocion] ADD  CONSTRAINT [DF_Pro_Promocion_Modificada]  DEFAULT ((1)) FOR [Modificada]
GO
ALTER TABLE [dbo].[Pro_Promocion] ADD  CONSTRAINT [DF_Pro_Promocion_Link]  DEFAULT ('') FOR [Link]
GO
ALTER TABLE [dbo].[Pro_Promocion] ADD  CONSTRAINT [DF_Pro_Promocion_Destacado]  DEFAULT ((0)) FOR [Destacado]
GO
ALTER TABLE [dbo].[Pro_Promocion] ADD  CONSTRAINT [DF_Pro_Promocion_Tipo]  DEFAULT ((2)) FOR [Tipo]
GO
